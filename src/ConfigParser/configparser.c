/*
 * Copyright (C) 2020 Wouter Loeve <wouterloeve0@gmail.com>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>

#include "../main.h"
#include "../Utils/global_structs.h"
#include "configparser.h"
#include "../Utils/strings.h"
#include "../Utils/log.h"

/*
 * Parses the configuration file.
 */

bool debug = false;

#define INVALID_VAL 1
#define SUCCESS_VAL 0
#define LINE_LENGTH 256

/*
 * Main function for this file, loops over the lines in the configuration file to parse them.
 */
int parseConfig(SimulationConfig *config) {
    FILE *configfilePtr = fopen(config->configfile, "r");
    if (configfilePtr == NULL) {
        fprintf(stderr, "Error opening file %s\n", config->configfile);
        return INVALID_VAL;
    }
    char line[LINE_LENGTH];
    while (fgets(line, sizeof(line), configfilePtr)) {
        if (strcmp(line, "") == 0) {
            continue;
        }
        int result = parseLine(config, line); 
        if (result == INVALID_VAL) {
            return INVALID_VAL;
        }
    }

    fclose(configfilePtr);
    return SUCCESS_VAL;
}

/*
 * Parses a line in the configuration file.
 */
int parseLine(SimulationConfig *config, char *line) {
    char* warn = calloc(1, sizeof(char) * (LINE_LENGTH + 100));
    const char delim[2] = "=";
    char linecpy[LINE_LENGTH];
    memset(linecpy, '\0', sizeof(linecpy));
    strcpy(linecpy, line);
    char *name = strsep(&line, delim);
    if (name == NULL) {
        sprintf(warn, "Could not get name from string \'%s\'\n", linecpy);
        WARNING("configparser", warn);
        warn = NULL;
        return INVALID_VAL;
    }
    name = trimwhitespace(name);

    char *value = strsep(&line, delim);
    if (name == NULL) {
        sprintf(warn, "Could not get value from string \'%s\'\n", linecpy);
        WARNING("configparser", linecpy);
        warn = NULL;
        return INVALID_VAL;
    }
    value = trimwhitespace(value);

    if (parseOption(config, name, value) == INVALID_VAL) {
        return INVALID_VAL;
    }
    free(warn);
    return SUCCESS_VAL;
}

/*
 * Sets the option corresponding to its name and value given in the configuration file.
 */
int parseOption(SimulationConfig *config, char *name, char* value) {
    char *warn = calloc(1, sizeof(char) * (LINE_LENGTH + 100));

    if (strcmp(name, "numThreads") == 0) {
        config->numThreads = string_to_int(value);
    } else if (strcmp(name, "debug") == 0) {
        if (strcmp(value, "true") == 0) {
            debug = true;
        } else if (strcmp(value, "false") == 0) {
            debug = false;
        } else {
            sprintf(warn, "Did not understand value \'%s\' in name \'%s\' \n", value, name);
            WARNING("configparser", warn);    
            warn = NULL;
            return INVALID_VAL;
        }
    } else if (strcmp(name, "sleepTime") == 0) {
        // Miliseconds
        config->commSleepTime = (unsigned int) strtol(value, (char **)NULL, 10);
    } else if (strcmp(name, "timeOut") == 0) {
        config->commTimeOut = (unsigned int) strtol(value, (char **)NULL, 10);
    } else if (strcmp(name, "heartbeatTries") == 0) {
        config->heartbeatTries = string_to_int(value);
    } else if (strcmp(name, "heartbeatCheckerPrio") == 0) {
        config->heartbeatCheckerPrio = string_to_int(value);
    } else if (strcmp(name, "heartbeatWorkerPrio")== 0) {
        config->heartbeatWorkerPrio = string_to_int(value);
    } else if (strcmp(name, "diagnosticsOutput") == 0) {
        config->diagnosticsOutput = strcpy_alloc(config->diagnosticsOutput, value);
    } else if (strcmp(name, "standbyEarlyTaskCompletion") == 0) {
        if (strcmp(value, "true") == 0) {
            config->standbyEarlyTaskCompletion = true;
        } else if (strcmp(value, "false") == 0) {
            config->standbyEarlyTaskCompletion = false;
        } else {
            sprintf(warn, "Did not understand value \'%s\' in name \'%s\' \n", value, name);
            ERROR("configparser", warn);    
            warn = NULL;
            return INVALID_VAL;
        }
    } else {
        sprintf(warn, "Did not understand option \'%s\'\n", name);
        ERROR("configparser", warn);
        warn = NULL;
        return INVALID_VAL;
    }

    free(warn);

    return SUCCESS_VAL;
}
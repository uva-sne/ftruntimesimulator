/*
 * Copyright (C) 2020 Wouter Loeve <wouterloeve0@gmail.com>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>

/*
 * Parses the thread configuration.
 * The thread configuration contains the assignments of components to threads in order to simulate spatial mappings of components to threads.
 */

#include "threadConfigParser.h"
#include "../Utils/strings.h"
#include "../Utils/log.h"

#define INVALID_VAL 1
#define SUCCESS_VAL 0
#define LINE_LENGTH 100

/*
 * Initialises the thread configuration data structure.
 */
ThreadConfig *initThreadConfig(SimulationConfig *config) {
    ThreadConfig *thConfig = malloc(sizeof(ThreadConfig));
    assert(thConfig);
    thConfig->items = malloc(sizeof(ThreadConfigItem*) * config->numThreads);
    assert(thConfig->items);
    thConfig->size = config->numThreads;
    for (int i = 0; i < config->numThreads; i++) {
        ThreadConfigItem *newItem = malloc(sizeof(ThreadConfigItem));
        assert(newItem);
        newItem->threadId = i;
        newItem->class = strcpy_alloc(newItem->class, "");
        newItem->componentNames = list_init();
        thConfig->items[i] = newItem;
    }

    return thConfig;
}

/*
 * Uses a configuration data structure to get the file and returns a thread configuration with the parsed contents of the thread configuration.
 */
ThreadConfig *parseThreadConfig(SimulationConfig *config) {
    ThreadConfig *thConfig = initThreadConfig(config);
    // We keep a list of assigned components to see that they are not assigned twice or not assigned at all.
    list_linked *assignedComponents = list_init();

    FILE *thConfigFilePtr = fopen(config->thConfigFile, "r");
    if (thConfigFilePtr == NULL) {
        fprintf(stderr, "Error opening file %s\n", config->thConfigFile);
        return NULL;
    }
    char line[LINE_LENGTH];
    while (fgets(line, sizeof(line), thConfigFilePtr)) {
        if (strcmp(line, "") == 0) {
            continue;
        }
        int result = parseThreadConfigLine(config, thConfig, line, assignedComponents); 
        if (result == INVALID_VAL) {
            return NULL;
        }
    }
    fclose(thConfigFilePtr);
    return thConfig;
}

int addThreadConfigItem(ThreadConfig *thConfig, int threadId, char *class, char *componentName, list_linked *assignedComponents) {
    ThreadConfigItem *cur = thConfig->items[threadId]; 
    assert(threadId == cur->threadId);
    if (strcmp(cur->class, "") == 0) {
        cur->class = trimwhitespace(strcpy_alloc(cur->class, class));
    }
    char *dest = NULL;
    if (strcmp(componentName, "") != 0) {
        dest = strcpy_alloc(dest, componentName);
        list_add_back(cur->componentNames, dest);
        list_add_back(assignedComponents, dest);
    }
    return SUCCESS_VAL;
}

/*
 * Creates and returns a token seperated by the given delimitter.
 */
char *createToken(char *warn, char **line, const char *delim, char *linecpy, char *argname) {
    char *name = strsep(line, delim);
    if (name == NULL) {
        sprintf(warn, "Could not get %s from string \'%s\'\n", argname, linecpy);
        WARNING("threadConfigParser", warn);
        warn = NULL;
        return NULL;
    }
    name = trimwhitespace(name);
    return name;
}

/*
 * Parses a line in the thread configuration.
 */
int parseThreadConfigLine(SimulationConfig *config, ThreadConfig *thConfig, char *line, list_linked *assignedComponents) {
    char* warn = calloc(1, sizeof(char) * (LINE_LENGTH + 100));
    const char delim1[2] = " ";
    const char delim2[2] = "=";
    char linecpy[LINE_LENGTH];
    memset(linecpy, '\0', sizeof(linecpy));
    strcpy(linecpy, line);
    // Strsep in function updates the pointer, but if we have a single pointer, it cannot be returned
    char** l = &line;

    char *class = createToken(warn, l, delim1, linecpy, "Class");
    assert(class);

    char *name = createToken(warn, l, delim2, linecpy, "ThreadId");
    assert(name);

    char *value = createToken(warn, l, delim2, linecpy, "ComponentId");
    
    int threadId = (int) strtol(name, (char **)NULL, 10);
    assert(threadId < config->numThreads);
    
    addThreadConfigItem(thConfig, threadId, strdup(class), value, assignedComponents);
    free(warn);
    return SUCCESS_VAL;
}
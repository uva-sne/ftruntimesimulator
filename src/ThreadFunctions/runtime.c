/*
 * Copyright (C) 2020 Wouter Loeve <wouterloeve0@gmail.com>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "runtime.h"

#include <semaphore.h>
#include "../Utils/pthreads.h"
#include <time.h>
#include "../Utils/time.h"
#include "../Utils/cleanup.h"

/*
 *************************************
 *  Start MasterThread Functions
 *************************************
 */

bool checkThreadOutputs(MainStruct *mainInfo, bool onlyRoot) {
	DEBUG("main", "Checking finished components");
	char *dbg = malloc(sizeof(char) * 100);
	assert(mainInfo);

	ReadyComponents *ready = mainInfo->ready;
	ready->readyIndex = 0;

	/* Go over the tasks that are finished, 
	 * Check whether their:
	 * - direct predecessors
	 * - direct successors
	 * - Themselves
	 * Are ready.
	 */
	GraphStruct *cur = NULL;
	int compId = -1;
	DEBUG("main", "Locking finished component lock");
	int e = pthread_mutex_lock(mainInfo->finished->readyLock);
	handleMutexError(mainInfo->finished->readyLock, e);
	bool anyDone = (mainInfo->finished->readyIndex > 0);
	assert(mainInfo->finished->readyIndex <= mainInfo->finished->size);

	// Go over the finished components and add them to the ready list
	for (int i = 0; i < mainInfo->finished->readyIndex; i++) {
		compId = mainInfo->finished->componentIds[i];
		cur = mainInfo->taskGraph->taskList[compId];
		cur->inQueue = false;
		mainInfo->finished->componentIds[i] = -1;

		checkSuccessorReady(cur, ready);
		checkPredecessorReady(cur, ready);

		// Check yourself
		if (cur->inReady == false) {
			addComponentToReady(cur, ready);
		}
	}
	mainInfo->finished->readyIndex = 0;
	pthread_mutex_unlock(mainInfo->finished->readyLock);
	DEBUG("main", "Unlocking finished component lock");

	if (onlyRoot) {
		// Add the root components to the ready queue iff they are ready and output buffers aren't full.
		list_node * curNode = task_graph_get_roots(mainInfo->taskGraph);
		cur = NULL;
		for (int i = 0; i < task_graph_get_num_roots(mainInfo->taskGraph); i++) {
			cur = list_get_data(curNode);
			assert(cur);

			if (cur->inReady == false) {
				addComponentToReady(cur, ready);
			}
			curNode = list_get_next(curNode);
		}
	}

	// Add the ready components to the appropriate task queue queue.
	for (int i = 0; i < ready->readyIndex; i++) {
		if (ready->componentIds[i] >= 0) {
			cur = mainInfo->taskGraph->taskList[ready->componentIds[i]];
			// fprintf(stderr, "ready: %s\n", cur->componentName);
			addToTaskQueues(cur, mainInfo);
			cur->inReady = false;
			ready->componentIds[i] = -1;
		}
	}
	free(dbg);

	return anyDone;
}

/*
 * Adds the component to the task queues of the threads by popping the required the tokens from the edges on the component inports.
 */
void addToTaskQueues(GraphStruct *node, MainStruct *mainInfo) {
	char *dbg = malloc(sizeof(char) * 100);
	sprintf(dbg, "Adding task %s", node->componentName);
	DEBUG("main", dbg);
	TaskStruct *prevTask = NULL;
	ThreadStruct *curThread = NULL;
	ThreadStruct *prevThread = mainInfo->threadInfos[node->threadId[0]];
	
	TaskStruct *task = take_output(node, node->prevEdges, mainInfo, mainInfo->errorToken, false);
	prevTask = task;
	if (task == NULL) {
		// Task has error token and has been stored already
		addToFinished(mainInfo->finished, node->componentId, mainInfo->diagnostics);
		mainInfo->diagnostics->errorTokenComponentFreqs[node->componentId]++;
		return;
	}


	if (node->threadIdSize > 1) {
		time_startPrimaryBackup(mainInfo->diagnostics->time);
		// Loop trough the threads assigned to this component and add a copy to each.
		for (int i = 0; i < node->threadIdSize; i++) {
			curThread = mainInfo->threadInfos[node->threadId[i]];

			if (i > 0) {
				if (profile_get_standby(node->profile)) {
					task = task_graph_copy_task(
						prevTask, 
						copy_comp_data(prevTask->inputData, prevTask->componentId), 
						alloc_output_struct(prevTask->componentId)
					);
				}

				assert(task != prevTask);

			}
			
			// We always add the previous task (unless there is only one, as we need to make sure we do not copy from a task that is already computing)
			if (i > 1) {
				addToTaskQueue(prevThread, prevTask);
				node->inQueue = true;
			}
			prevTask = task;
			prevThread = curThread;
		}
		time_stopPrimaryBackup(mainInfo->diagnostics->time);
		addToTaskQueue(prevThread, prevTask);
		node->inQueue = true;
	} else {
		addToTaskQueue(mainInfo->threadInfos[node->threadId[0]], task);
		node->inQueue = true;
	}

	free(dbg);
}

/*
 * Adds a single component to the task queues of the specific thread
 */ 
void addToTaskQueue(ThreadStruct *thread, TaskStruct *task) {
	int e = pthread_mutex_lock(thread->taskQueueLock);
	handleMutexError(thread->taskQueueLock, e);
		assert(list_add_back(thread->taskQueue, task) == 0);
	pthread_mutex_unlock(thread->taskQueueLock);
	sem_post(thread->threadSemaphore);
}

/*
 * Checks whether the successors of this component are ready to fire and adds them to the task queue by calling addComponenToReady.
 */
void checkSuccessorReady(GraphStruct *component, ReadyComponents *ready) {
	list_linked *edges = component->nextEdges;
	EdgeStruct *cur = NULL;
	GraphStruct *curGS = NULL;
	list_node *curNode = list_get_first(edges);
	for (int i = 0; i < edges->size; i++) {
		cur = list_get_data(curNode);

		for (int out = 0; out < cur->outputIndex; out++) {
			curGS = cur->outports[out]->graph;
			addComponentToReady(curGS, ready);
		}

		curNode = list_get_next(curNode);
	}
}

/*
 * Checks whether the predecessors of this component are ready to fire and adds them to the task queue by calling addComponenToReady.
 */
void checkPredecessorReady(GraphStruct *component, ReadyComponents *ready) {
	list_linked *edges = component->prevEdges;
	EdgeStruct *cur = NULL;
	list_node *curNode = list_get_first(edges);
	for (int i = 0; i < edges->size; i++) {
		cur = list_get_data(curNode);

		addComponentToReady(cur->input, ready);

		curNode = list_get_next(curNode);
	}
}

/*
 * Adds a component to the given ready queue.
 */
void addComponentToReady(GraphStruct *curGS, ReadyComponents *ready) {
	if (curGS->inReady == false && curGS->inQueue == false && task_graph_ready_to_fire(curGS)) {
		// fprintf(stderr, "Component %s is ready\n", curGS->componentName);
		ready->componentIds[ready->readyIndex] = curGS->componentId;
		ready->readyIndex++;
		curGS->inReady = true;
		assert(ready->readyIndex <= ready->size);
	}
}

/*
 * Main functions for the main or control thread.
 */
void *mainThread(void *minfo) {
	MainStruct *mainInfo = (MainStruct *) minfo;

	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
	
	time_startTime(mainInfo->diagnostics->time->startTotal);
	DEBUG("main", "Main start");


	int e = pthread_mutex_lock(mainInfo->usingTaskQueue);
	handleMutexError(mainInfo->usingTaskQueue, e);
			checkThreadOutputs(mainInfo, true);
	pthread_mutex_unlock(mainInfo->usingTaskQueue);

	while (true) {
		DEBUG("main", "Main lock");

		// Lock the task queues and check whether any threads are done and adds new components to the ready queue.
		int e = pthread_mutex_lock(mainInfo->usingTaskQueue);
		handleMutexError(mainInfo->usingTaskQueue, e);
			checkThreadOutputs(mainInfo, false);
		pthread_mutex_unlock(mainInfo->usingTaskQueue);

		DEBUG("main", "cond wait");
		
		// Wait for threads to finish components so that the task queue can be checked for newly ready components.
		time_startTime(mainInfo->diagnostics->time->startGeneral);
		pthread_mutex_lock(mainInfo->finished->readyLock);
		while (mainInfo->finished->readyIndex == 0) {
			pthread_cond_wait(mainInfo->signalCondition, mainInfo->finished->readyLock);
		}
		pthread_mutex_unlock(mainInfo->finished->readyLock);
		time_stopWaiting(mainInfo->diagnostics->time);

		DEBUG("main", "Unlocking, relooping");
	}
}

/*
 *************************************
 *  Start ComputationThread Functions
 *************************************
 */

void addToFinished(ReadyComponents *finished, int componentId, Diagnostics *diag) {
	int e = pthread_mutex_lock(finished->readyLock);
	handleMutexError(finished->readyLock, e);
	pthread_cleanup_push(mutex_cleanup_handler, finished->readyLock);
	// fprintf(stderr, "INDEX: %i / %i\n", info->finished->readyIndex, info->finished->size);
	assert(finished->readyIndex < finished->size);
	finished->componentIds[finished->readyIndex] = componentId;
	finished->readyIndex++;
	diag->firedComponentFreqs[componentId]++;
	// pthread_mutex_unlock(finished->readyLock);
	pthread_cleanup_pop(1);
}

GraphStruct *getComponentFromTask(MainStruct *mainInfo, TaskStruct *task) {
	return mainInfo->taskGraph->taskList[task->componentId];
}

bool compThreadRetrieveTask(ThreadStruct *info, char *loc, char *dbg) {
	list_node *newTaskNode;
	GraphStruct *curNode;
	bool success = true;
	
	TaskStruct *task;

	DEBUG(loc, "Locking Taskqueue");
	int e = pthread_mutex_lock(info->taskQueueLock);
	handleMutexError(info->taskQueueLock, e);
	pthread_cleanup_push(mutex_cleanup_handler, info->taskQueueLock);
		DEBUG(loc, "Locked Taskqueue");
		newTaskNode = list_pop_first(info->taskQueue);
		task = (TaskStruct *) list_get_data(newTaskNode);

		sprintf(dbg, "Taking out task with component %s, id: %i", task->componentName, task->componentId);
		DEBUG(loc, dbg);
		sprintf(dbg, "Taskqueue size %i", info->taskQueue->size);
		DEBUG(loc, dbg);
	
		list_free_node(newTaskNode); 
		info->currentTask = task;
		curNode = task_graph_get_gs_by_id(info->mainInfo->taskGraph, task->componentId);
	pthread_cleanup_pop(1);

	if (info->mainInfo->config->standbyEarlyTaskCompletion) {
		/*
		* If it is already delivered why should we still compute.
		*/
		e = pthread_mutex_lock(curNode->nodeMutex);
		handleMutexError(curNode->nodeMutex, e);
		pthread_cleanup_push(mutex_cleanup_handler, curNode->nodeMutex);
			if (*info->currentTask->delivered > 0) {
				success = false;
				*info->currentTask->delivered += 1 ;
			}
		// Unlocks mutex
		pthread_cleanup_pop(1);
	}

	return success;
}

void compThreadStoreTask(GraphStruct *curComp, ThreadStruct *info) {
	// Lock the node because we cannot deliver two tasks at the same time and increase the delivered var.
	int e = pthread_mutex_lock(curComp->nodeMutex);
	handleMutexError(curComp->nodeMutex, e);
	pthread_cleanup_push(mutex_cleanup_handler, curComp->nodeMutex);
		if (*info->currentTask->delivered == 0) {
			// We're only interested in wasted computation, the first delivery is not wasted.
			info->time->isUsingPrimaryBackup = false;
			// Delivery is the same under normal operation
			// assert(curComp->inQueue);
			// Has mutex lock build in the edge
			store_output(info->mainInfo, info->currentTask, info->time);

			addToFinished(info->finished, info->currentTask->componentId, info->mainInfo->diagnostics);
			pthread_cond_signal(info->mainInfo->signalCondition);
			if (*info->currentTask->checkpointed == true) {
				DEBUG("THREAD", "CHECKPOINT WORKED");
			}
			
			// Remove checkpointed data from edge since computation was successful
			if (*info->currentTask->checkpointed == false && profile_get_checkpoint(curComp->profile)) {
				time_startCheckpoint(info->time);
				void *out = take_output(curComp, curComp->prevEdges, info->mainInfo, info->mainInfo->errorToken, true);
				if (out != NULL) {
					free(out);
				}
				
				*info->currentTask->checkpointed = true;
				time_stopCheckpointRestart(info->time);
			}
		} else {
			assert(profile_get_standby(curComp->profile));

			time_stopPrimaryBackup(info->time);
			
		}
		*info->currentTask->delivered += 1 ;
		// Invalidate timer as we're only interested in wasted time
		info->time->isUsingCheckpoint = false;

		profile_get_checkpoint(curComp->profile) ? assert(*info->currentTask->checkpointed == true) : (void)true;
	// Unlocks mutex
	pthread_cleanup_pop(1);
}

void *compThread(void* arg) {
	// Thread will die 'immediately' when canceled
	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

	ThreadStruct *info = (ThreadStruct *) arg;
	time_startTime(info->time->startTotal);
	pthread_cleanup_push(thread_cleanup, info);
	bool retrievalSuccessful = false;

	char *loc = calloc(1, sizeof(char) * (100));
	char *dbg = calloc(1, sizeof(char) * (100));
	sprintf(loc, "Computation Thread id: %i", info->thread_id);
	DEBUG(loc, "Start");
	
	GraphStruct *curComp;
	int replicas = 0;

	while (true) {
		// Wait until there is something in the task queue.
		DEBUG(loc, "Waiting for task queue");
		
		time_startTime(info->time->startGeneral);
		sem_wait(info->threadSemaphore);
		time_stopWaitingThread(info->time);
		time_startCheckpoint(info->time);

		// Take first task from task queue since it is non-empty
		retrievalSuccessful = compThreadRetrieveTask(info, loc, dbg);
		if (retrievalSuccessful == false) {
			continue;
		}
		curComp = getComponentFromTask(info->mainInfo, info->currentTask);
		if (profile_get_standby(curComp->profile)) {
			time_startPrimaryBackup(info->time);
		}
		replicas = profile_get_num_replicas(curComp->profile);
		(void)replicas;

		DEBUG(loc, "Calling Compute thread");
		compFunc(info->currentTask);
		DEBUG(loc, "Finished computing");
		info->currentTask->done = true;

		// Store threads' results into the graph
		DEBUG(loc, "Storing results in graph");
		if (info->currentTask != NULL) {
			compThreadStoreTask(curComp, info);

			// We save a temp because need to set it to 
			// NULL first, so that we cannot access the free'd data when cancelling the thread
			TaskStruct *temp = info->currentTask;
			info->currentTask = NULL;

			if (*temp->delivered == replicas + *temp->crashed) {
				free(temp);
			}

			DEBUG(loc, "Alerting main thread");
		}
	}
	pthread_cleanup_pop(1);
}

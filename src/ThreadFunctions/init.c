/*
 * Copyright (C) 2020 Wouter Loeve <wouterloeve0@gmail.com>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdlib.h>
#include <pthread.h>

#include "init.h"
#include "../Utils/log.h"
#include "../Utils/pthreads.h"
#include "../Utils/time.h"
#include "../generated/generated.h"
#include <semaphore.h>
#include "runtime.h"
#include "heartbeat.h"
#include "../Datastructures/taskgraph.h"

#include <sys/time.h>

/*
 * Initialises a ReadyCOmponents data structures which is an array with sizes with locks included. 
 * These are used to communicate the components that are finished from the worker thread to the main thread.
 */
ReadyComponents *initReadyComp(int numComps) {
	ReadyComponents *ready = malloc(sizeof(ReadyComponents));
	assert(ready);
	ready->size = numComps;
	ready->componentIds = malloc(sizeof(int) * ready->size);
	ready->readyIndex = 0;
	ready->readyLock = createPthreadMutex();
	return ready;
}

/*
 * Retrieves a thread config item with the given ID by looping.
 */
ThreadConfigItem *getThreadConfItemById(ThreadConfig *thConfig, int id) {
	ThreadConfigItem *cur = NULL;
    for (int i = 0; i < thConfig->size; i++) {
        cur = thConfig->items[i];

		if (cur->threadId == id) {
			return cur;
		}
    }

	return NULL;
}

/*
 * Intialises the diagnostics datastructure
 * 
 */
Diagnostics *initDiagnostics(MainStruct *mainInfo) {
	Diagnostics *d = malloc(sizeof(Diagnostics));
	assert(d);
	d->crashedComponents = malloc(sizeof(int) * mainInfo->config->numThreads);
	assert(d->crashedComponents);
	d->numCrashed = 0;
	d->numCheckpoint = 0;
	d->numPrimaryBackup = 0;
	if (mainInfo->config->diagnosticsOutput == NULL) {
		d->logFile = NULL;
		d->logFileLoc = NULL;
	} else {
		d->logFileLoc = mainInfo->config->diagnosticsOutput;
		d->logFile = fopen(d->logFileLoc, "w+");
		assert(d->logFile);
	}
	d->firedComponentFreqs = malloc(sizeof(int) * mainInfo->taskGraph->numComps);
	d->time = time_initTimeThread();
	assert(d->firedComponentFreqs);
	d->errorTokenComponentFreqs = malloc(sizeof(int) * mainInfo->taskGraph->numComps);
	assert(d->errorTokenComponentFreqs);

	d->savedComponents = malloc(sizeof(int) * mainInfo->taskGraph->numComps);
	assert(d->savedComponents);

	d->directlyCrashedComps = malloc(sizeof(int) * mainInfo->taskGraph->numComps);
	assert(d->directlyCrashedComps);

	for (int i = 0; i < mainInfo->taskGraph->numComps; i++) {
		d->firedComponentFreqs[i] = 0;
		d->errorTokenComponentFreqs[i] = 0;
		d->directlyCrashedComps[i] = 0;
		d->savedComponents[i] = 0;
	}
	return d;
}

/*
 * Adds the thread ID to the GraphStruct if class is correct.
 * Used during thread configuration validation.
 */
void addThreadId(task_graph *taskGraph, ThreadConfig *thConfig, ThreadConfigItem *currentConfig, GraphStruct *gs, char *dbg) {
	ThreadConfigItem *reference;
	int threadId = currentConfig->threadId;

	char *expectedClass = NULL;
	
	if (gs->threadIdSize > 0) {
		reference = getThreadConfItemById(thConfig, gs->threadId[0]);
		assert(reference);
		expectedClass = reference->class;
		if (! strcmp(currentConfig->class, expectedClass) == 0) {
			sprintf(dbg, 
				"Cannot assign %s to %i due to class difference in component, expected %s but got %s", 
				gs->componentName, threadId, expectedClass, currentConfig->class
			);
			ERROR("init.c", dbg);
		}
	}
	task_graph_set_thread_id(gs, threadId);
	sprintf(dbg, "Assigning component %s to thread %i\n", 
		gs->componentName,
		currentConfig->threadId
	);
	DEBUG("init.c", dbg);
}

/*
 * Checks whether a component has enough replicas assigned in the thread configuration
 */
void checkReplicas(task_graph *taskGraph) {
	char *dbg = malloc(sizeof(char) * 250);
	GraphStruct *cur = NULL;
	for (int i = 0; i < taskGraph->numComps; i++) {
		cur = taskGraph->taskList[i];
		if (cur->threadIdIndex != cur->threadIdSize) {
			sprintf(dbg, 
			"Component %s does not have sufficient replicas assigned, expected %i, got %i\n", 
			cur->componentName, cur->threadIdSize, cur->threadIdIndex);
			ERROR("init.c", dbg);
		}
	}
}

/*
 * Validates the thread configuration.
 * Components can only be assigned to threads which have the right class.
 * The component class is the class to the thread class to which the first component of this class is defined
 * Every component needs to be assigned.
 * In standby configuration is the number of required replicas equal to the replica amount in the coordination language + 1. (TODO change this to remove the +1).
 * In case of multiple replicas, the same component name cannot be assigned to the same thread.
 */
bool validateThreadGraph(task_graph *taskGraph, ThreadConfig *thConfig, bool printConfig) {
	char *dbg = malloc(sizeof(char) * 250);
	char *currentConfigComp = NULL;
	list_node *curNode = NULL;
	list_node *curNodeInner = NULL;
	bool found = false;
	GraphStruct *gs = NULL;
	ThreadConfigItem *currentConfig;

	// Loop trough the thread configurations
	for (int configComp = 0; configComp < thConfig->size; configComp++) {
		currentConfig = thConfig->items[configComp];
		int length = currentConfig->componentNames->size;
		curNode = list_get_first(currentConfig->componentNames);
		// Loop trough the components assigned to the current thread
		for (int i = 0; i < length; i++ ) {
			currentConfigComp = (char*) list_get_data(curNode);

			// Find whether the component really exists.
			if (strcmp(currentConfigComp, "") != 0) {
				gs = task_graph_get_gs_by_name(taskGraph, currentConfigComp);
				assert(gs);
				found = true;
				addThreadId(taskGraph, thConfig, currentConfig, gs, dbg);
			}

			int numEqualAssignedComps = 0;
			curNodeInner = list_get_first(currentConfig->componentNames);
			for (int j = 0; j < length; j++) {
				char *innerConfigComp = (char *) list_get_data(curNodeInner);
				if (strcmp(innerConfigComp, currentConfigComp) == 0) {
					numEqualAssignedComps++;
				}
				curNodeInner = curNodeInner->next;
			}

			assert(numEqualAssignedComps == 1);

			if (found == false && strcmp(currentConfigComp, "") != 0 ) {
				char *warn = malloc(sizeof(char *) * 100);
				sprintf(warn, "Component name \'%s\' not found - Thread Config not valid", currentConfigComp);
				WARNING("Init.c", warn);
				return false;
			}
			curNode = list_get_next(curNode);
		} 


		found = false;

	}

	checkReplicas(taskGraph);
	if (printConfig) {
		printf("Config validation finished, exiting....\n");
		exit(0);
	}
	free(dbg);
	return true;
}

/*
 * Initialises the global datastructures used by all threads.
 */
MainStruct *initDatastructures(SimulationConfig *config, ThreadConfig *thConfig) {
	MainStruct *mainInfo = malloc(sizeof(MainStruct));
	mainInfo->config = config;
	mainInfo->thConfig = thConfig;
	mainInfo->signalCondition = createPthreadCond();

	mainInfo->threadInfos = calloc(config->numThreads, sizeof(ThreadStruct*));
	mainInfo->commInfos = malloc(config->numThreads * sizeof(CommunicationThread*));
	assert(mainInfo->threadInfos && mainInfo->commInfos);
	// mainInfo->threadInfosLock = createPthreadMutex();
	// mainInfo->inputCondition = createPthreadCond();
	mainInfo->usingTaskQueue = createPthreadMutex();
	
	mainInfo->taskGraph = generate_task_graph();
	// Check whether the names in the graph correspond with the names used in the config
	assert(validateThreadGraph(mainInfo->taskGraph, mainInfo->thConfig, config->printConfig));

	mainInfo->ready = initReadyComp(mainInfo->taskGraph->numComps);
	mainInfo->finished = initReadyComp(mainInfo->taskGraph->numComps);

	// Each computation thread has a buddy communication thread
	// Furthermore there is one master thread and 1 master comm thread.
	mainInfo->numThreads = config->numThreads * 2 + 2;
	mainInfo->errorToken = malloc(sizeof(bool));
	assert(mainInfo->errorToken);
	mainInfo->diagnostics = initDiagnostics(mainInfo);

	return mainInfo;
}

/*
 * Initialises data structures necessary for a computation thread and launches the thread.
 */
void createComputationThread(int i, int buddy_id, ThreadStruct *threadInfo, MainStruct *mainInfo) {
	int error;
	if (threadInfo == NULL) {
		ERROR("init.c", "Could not allocation threadInfo struct");	
	}
	threadInfo->config = mainInfo->config;
	threadInfo->thread_id = i;
	threadInfo->buddy_id = buddy_id;

	threadInfo->inputCondition = mainInfo->inputCondition;

	threadInfo->outputLock = createPthreadMutex();
	threadInfo->outputData = NULL;

	threadInfo->taskQueue = list_init();
	threadInfo->hasTaskQueueLock = false;
	threadInfo->taskQueueLock = createPthreadMutex();

	threadInfo->signalCondition = mainInfo->signalCondition;
	threadInfo->threadSemaphore = createSemaphore(0);

	threadInfo->finished = mainInfo->finished;
	threadInfo->mainInfo = mainInfo;

	threadInfo->time = time_initTimeThread();

	threadInfo->crashed = false;
	
	// Computation thread
	error = pthread_create(mainInfo->tids[i], NULL, &compThread, (void*) threadInfo);
	if (error) {
		ERROR("init.c", "Could not create computation thread.");
	}
}

/*
 * Initialises data structures necessary for a communication thread and launches the thread.
 */
void createCommunicationThread(int buddy, int i, CommunicationThread *commInfo, MainStruct *mainInfo) {
	commInfo->buddyId = buddy;
	commInfo->threadId = i;
	commInfo->timeLock = createPthreadMutex();
	commInfo->sleepTime = mainInfo->config->commSleepTime;
	commInfo->curTime = malloc(sizeof(struct timespec));
	commInfo->crashed = false;
	assert(commInfo->curTime);

	pthread_attr_t *attr = createRoundRobinAttr();
	int error = pthread_create(mainInfo->tids[i], attr, &commThread, (void*) commInfo);
	if (error) {
		ERROR("init.c", "Could not create communication thread.");
	}
	pthread_setschedprio(*mainInfo->tids[i], mainInfo->config->heartbeatWorkerPrio);
}

pthread_t *allocateThread() {
	pthread_t * tid = malloc(sizeof(pthread_t));
	assert(tid);
	return tid;
}

/*
 * Initialises the number of communication threads as indicated in the configuration file. The main/master/control thread and main heartbeat thread are also created here.
 */
MainStruct *initThreads(MainStruct *mainInfo) {
	SimulationConfig *config = mainInfo->config;
	/* num_threads is the number of execution threads
	 * Each execution thread has a communication thread for the heartbeat
	 * Then finally there is a master thread.
	 */
	pthread_t **tids = malloc(sizeof(pthread_t *) * mainInfo->numThreads);
	mainInfo->tids = tids;

	ThreadStruct **threadInfos = mainInfo->threadInfos;
	CommunicationThread **commInfos = mainInfo->commInfos;

	for (int i = 0; i < config->numThreads; i++) {
		ThreadStruct *threadInfo = calloc(1, sizeof(ThreadStruct));
		CommunicationThread *commInfo = calloc(1, sizeof(CommunicationThread));
		assert(threadInfo && commInfo);

		mainInfo->tids[i] = allocateThread();
		int commId = config->numThreads + i;
		mainInfo->tids[commId] = allocateThread();
		
		createComputationThread(i, commId, threadInfo, mainInfo);
		createCommunicationThread(i, commId, commInfo, mainInfo);

		threadInfos[i] = threadInfo;
		commInfos[i] = commInfo;
	}

	// master comm thread
	int mainCommId = mainInfo->numThreads - 2;
	mainInfo->tids[mainCommId] = allocateThread();

	pthread_attr_t *attr = createRoundRobinAttr();
    int error = pthread_create((mainInfo->tids[mainCommId]), attr, &mainCommThread, (void*) mainInfo);
    if (error) {
        ERROR("main.c", "Could not create main heartbeat thread.");
    }
	pthread_setschedprio(*mainInfo->tids[mainCommId], mainInfo->config->heartbeatCheckerPrio);
    // Master thread
	int mainId = mainInfo->numThreads - 1;
	mainInfo->tids[mainId] = allocateThread();
    error = pthread_create((mainInfo->tids[mainId]), NULL, &mainThread, (void*) mainInfo);
    if (error) {
        ERROR("main.c", "Could not create main thread.");
    }


	return mainInfo;
}


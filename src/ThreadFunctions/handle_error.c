/*
 * Copyright (C) 2020 Wouter Loeve <wouterloeve0@gmail.com>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "handle_error.h"
#include <stdlib.h>

#include "../generated/generated.h"
#include "assert.h"
#include "../Utils/log.h"
#include "init.h"
#include "../Utils/arrays.h"
#include "../Utils/print.h"
#include "../Utils/pthreads.h"
#include "../Utils/cleanup.h"
#include "../ThreadFunctions/runtime.h"
#include "../Utils/print.h"

/*
 * This file contains functions for the crash handling and reconfiguration procedure which occurs when a crash error is detected.
 */

/*
 * Prints an error message with the indicated location in front and cleans up afterwards.
 * Cleanup retrieves the timing structs from all threads and exits gracefully.
 */
void ERROR_CLEANUP(char *loc, char *msg, MainStruct* mainInfo) {
	fprintf(stderr, "ERROR: in %s - %s\n", loc, msg);
	general_cleanup(mainInfo);
}

/*
 * Returns whether the first assigned component of the crashed thread already exists in the destination thread.
 */
bool checkAssignedDuplicateComponents(MainStruct *mainInfo, ThreadConfigItem *crashed, ThreadConfigItem *candidate) {
	GraphStruct *cur = NULL;
	DEBUG("Handle_error", "checkAssignedDuplicateComponents");
	char *compName = list_get_data(list_get_first(crashed->componentNames));
	cur = task_graph_get_gs_by_name(mainInfo->taskGraph, compName);
	assert(cur);
	if (hasMember(cur->threadId, cur->threadIdSize, candidate->threadId)) {
		return false;
	}

	return true;
} 

/*
 * Retrieves thread with the same class with fewest components assigned.
 * Also checks whether the newly assinged component doesn't already have the component assigned as replicas
 */
int getFewestAssignedThread(MainStruct *mainInfo, ThreadConfigItem *crashed) {
	ThreadConfigItem *currentConfig;
	ThreadStruct *candidate;
	int minIndex = -1;
	int minLength = INT16_MAX;
	for (int configComp = 0; configComp < mainInfo->thConfig->size; configComp++) {
		currentConfig = mainInfo->thConfig->items[configComp];
		int length = currentConfig->componentNames->size;

		bool notItself = (currentConfig->threadId != crashed->threadId);
		bool lowerLength = minLength > length;
		if (notItself && lowerLength && (strcmp(currentConfig->class, crashed->class) == 0)) {
			candidate = mainInfo->threadInfos[configComp];
			if (! candidate->crashed && checkAssignedDuplicateComponents(mainInfo, crashed, currentConfig)) {
				minLength = length;
				minIndex = configComp;
			}
		}
	}

	return minIndex;
}

/*
 * Invalidates the component executions in the queue of the crashed threads.
 */
list_linked *invalidateTasks(MainStruct *mainInfo, list_linked *tasks, ThreadStruct *crashed) {
	list_node *curNode = list_get_first(tasks);
	TaskStruct *cur = NULL;
	list_linked *recoverableTasks = list_init();
	TaskStruct *newTask = NULL;

	for (int i = 0; i < tasks->size; i++) {
		cur = list_get_data(curNode);

		newTask = invalidateTask(mainInfo, cur, crashed->finished);
		if (newTask) {
			list_add_back(recoverableTasks, newTask);
			newTask = NULL;
		}

		curNode = list_get_next(curNode);
	}

	return recoverableTasks;
}

/*
 * Invalidates a single item from the task queue of the task thread.
 * See the thesis document for details of the reconfiguration process.
 */
TaskStruct *invalidateTask(MainStruct *mainInfo, TaskStruct *task, ReadyComponents *finished) {
	TaskStruct *newTask = NULL;
	GraphStruct *curTask = task_graph_get_gs_by_id(mainInfo->taskGraph, task->componentId);
	bool unsavable = true;

	mainInfo->diagnostics->directlyCrashedComps[curTask->componentId] += 1;
	
	/* 
	 * If we have not checkpointed this task before and checkpoint restart is enabled
	 */
	if (*task->checkpointed == false && profile_get_checkpoint(curTask->profile)) {
		time_startCheckpoint(mainInfo->diagnostics->time);
		newTask = take_output(curTask, curTask->prevEdges, mainInfo, mainInfo->errorToken, true);
		*task->checkpointed = true;
		*newTask->checkpointed = true;
		mainInfo->diagnostics->numCheckpoint++;
		unsavable = false;

		time_stopCheckpointRestart(mainInfo->diagnostics->time);
		// If we can recover the task isn't finished.
	} else if (profile_get_standby(curTask->profile)) {
		// Using taskqueue lock prevents that this interferes with the main thread
		time_startPrimaryBackup(mainInfo->diagnostics->time);

		int e = pthread_mutex_lock(curTask->nodeMutex);
		handleMutexError(curTask->nodeMutex, e);
			*task->crashed += 1;
			if (*task->crashed < profile_get_num_replicas(curTask->profile)) {
				unsavable = false;
				mainInfo->diagnostics->savedComponents[curTask->componentId] += 1;
				mainInfo->diagnostics->numPrimaryBackup++;
			}
		pthread_mutex_unlock(curTask->nodeMutex);


		time_stopPrimaryBackup(mainInfo->diagnostics->time);

	}
	if (unsavable) {
		if ( ! task_graph_is_root(curTask)) {
			set_error(task);
			// Produce error token
			store_output(mainInfo, task, mainInfo->diagnostics->time);
			char *dbg = malloc(sizeof(char) * 100);
			assert(dbg);
			sprintf(dbg, "Invalidating task with componentName %s\n", task->componentName);
			DEBUG("handle_error", dbg);
			// Designate task as finished
			mainInfo->diagnostics->errorTokenComponentFreqs[task->componentId]++;
		}
		addToFinished(finished, task->componentId, mainInfo->diagnostics);
	}
	// Return newtask so it can be added to the reconfigured component.
	return newTask;
}

/*
 * Assigns the recoverable (= with fault-tolerance methods) to their new task runners.
 */
void assignRecoverableTasks(list_linked *recoverableTasks, MainStruct *mainInfo) {
	TaskStruct *recoverTask = NULL;
	ThreadStruct *newThread = NULL;
	list_node *curNode = list_get_first(recoverableTasks);
	GraphStruct *taskNode = NULL;
	char *dbg = malloc(sizeof(char) * 100);
	assert(dbg);

	for (int i = 0; i < recoverableTasks->size; i++) {
		recoverTask = list_get_data(curNode);
		taskNode = task_graph_get_gs_by_id(mainInfo->taskGraph, recoverTask->componentId);

		newThread = mainInfo->threadInfos[taskNode->threadId[0]];

		int e = pthread_mutex_lock(newThread->taskQueueLock);
		handleMutexError(newThread->taskQueueLock, e);
			list_add_back(newThread->taskQueue, recoverTask);
		pthread_mutex_unlock(newThread->taskQueueLock);
		
		sprintf(dbg, "Successfully recovered task with id %i, name %s\n", recoverTask->componentId, recoverTask->componentName);
		DEBUG("handle_error", dbg);
		sem_post(newThread->threadSemaphore);

		mainInfo->diagnostics->savedComponents[taskNode->componentId] += 1;

		curNode = list_get_next(curNode);
	}

}

int getNumLivingStandby(GraphStruct *node, MainStruct *mainInfo) {
	int result = 0;
	for (int i = 0; i < node->threadIdSize; i++) {
		int threadId = node->threadId[i];
		if (! mainInfo->threadInfos[threadId]->crashed) {
			result++;
		}
	}

	return result;
}	

/*
 * Reassign future tasks to non-failed components.
 */
void reRouteTasks(MainStruct *mainInfo, ThreadStruct *crashed, list_linked *recoverableTasks) {
	ThreadConfigItem *crashedItem = mainInfo->thConfig->items[crashed->thread_id];	
	ThreadConfigItem *newItem = NULL;
	char *dbg = malloc(sizeof(char) * 100);
	assert(dbg);
	

	// Size decreases so we have to save it here.
	int length = crashedItem->componentNames->size;

	list_node *curNode = list_get_first(mainInfo->threadInfos[crashed->thread_id]->taskQueue);
	TaskStruct *curStruct = NULL;
	for (int i = 0; i < mainInfo->threadInfos[crashed->thread_id]->taskQueue->size; i++) {
		curStruct = list_get_data(curNode);
		sprintf(dbg, "COMPONENT IN QUEUE %s\n\n", curStruct->componentName);
		DEBUG("handle_error", dbg);
		curNode = list_get_next(curNode);
	}

	curNode = list_get_first(crashedItem->componentNames);
	for (int i = 0; i < length; i++) {
		char *compName = list_get_data(curNode);
		GraphStruct *gs = task_graph_get_gs_by_name(mainInfo->taskGraph, compName);
		if (gs == NULL) {
			continue;
		}
		// assert(gs);
		sprintf(dbg, "REROUTING COMPONENT %s\n", compName);
		DEBUG("handle_error", dbg);
		
		// Find replacement for crashed thread for this component
		int newThreadIndex = getFewestAssignedThread(mainInfo, crashedItem);

		if (newThreadIndex == -1) {
			// We cannot reassign this one.
			if (profile_get_standby(gs->profile) && getNumLivingStandby(gs, mainInfo) >= 1 ) {
				// This is a spare standby component, we can just skip it
				list_pop_first(crashedItem->componentNames);
				continue;
			}
			char *dbg = malloc(sizeof(char) * 100);
			assert(dbg);
			sprintf(dbg, "Could not reconfig threadId %i in class %s \n", crashedItem->threadId, crashedItem->class);
			
			ERROR_CLEANUP("handle_error", dbg, mainInfo);
		}

		newItem = mainInfo->thConfig->items[newThreadIndex];
		// Update configuration in graph struct
		task_graph_recover(gs, crashedItem->threadId, newItem->threadId);

		sprintf(dbg, "REROUTED COMPONENT %s to %i\n", compName, newItem->threadId);
		DEBUG("handle_error", dbg);
		curNode = list_get_next(curNode);
		
		// Remove component from the current threadconfigitem
		list_node *prev = list_pop_first(crashedItem->componentNames);
		list_add_back(newItem->componentNames, list_get_data(prev));
	}

	// If there are tasks that are recovered, try to add them to the newly assigned thread.
	if (recoverableTasks->size > 0) {
		assignRecoverableTasks(
			recoverableTasks, 
			mainInfo
		);
	}

	free(dbg);
}

/*
 * Prints the assignments of components to threads.
 */
void printThreadAssignments(ThreadConfigItem *conf) {
	char *dbg = malloc(sizeof(char) * 100);
	assert(dbg);
	list_node *curNode = list_get_first(conf->componentNames);
	sprintf(dbg,"Printing thread assignments for %i with class %s\n", conf->threadId, conf->class);
	DEBUG("handle_error", dbg);

	for (int i = 0; i < conf->componentNames->size; i++) {
		char *compName = list_get_data(curNode);
		sprintf(dbg, "Componentname %s\n", compName);
		DEBUG("handle_error", dbg);
		curNode = list_get_next(curNode);
	}

	free(dbg);
}

/*
 * Parent function printThreadAssignments
 */
void printAllThreadAssignments(ThreadConfig *conf) {
	DEBUG("Handle_error", "Printing Thread Assignments");
	for (int i = 0; i < conf->size; i++) {
		printThreadAssignments(conf->items[i]);
	}
}

/*
 * Handles a crash fault in one of the threads.
 * Main or parent function of the reconfiguration process which is called by the main heartbeat thread.
 */
void handleCrashError(MainStruct *mainInfo, ThreadStruct *crashed, CommunicationThread *commInfo) {
	time_startReconfig(mainInfo->diagnostics->time);

	assert(crashed && crashed->crashed == false);
	// Add this thread to diagnostics
	mainInfo->diagnostics->crashedComponents[mainInfo->diagnostics->numCrashed] = crashed->thread_id;
	mainInfo->diagnostics->numCrashed++;

	char *dbg = malloc(sizeof(char) * 100);
	assert(dbg);
	
	crashed->crashed = true;
	commInfo->crashed = true;
	list_linked *recoverableTasks;
	TaskStruct *recoveredTask;
	
	sprintf(dbg, "Thread with id: %i, crashed\n", crashed->thread_id);
	DEBUG("handle_error", dbg);

	int e = pthread_mutex_lock(mainInfo->usingTaskQueue);
	handleMutexError(mainInfo->usingTaskQueue, e);

		printAllThreadAssignments(mainInfo->thConfig);

		recoverableTasks = invalidateTasks(mainInfo, crashed->taskQueue, crashed);
		// Cannot invalidate current task when it does not have one.
		if (crashed->currentTask != NULL) {
			// time_stopCheckpointRestart(crashed->time);
			// mainInfo->diagnostics->time->wastedTimeCheckpoint = time_endTime(mainInfo->diagnostics->time->startCheckpoint, mainInfo->diagnostics->time->stopTemp);
			recoveredTask = invalidateTask(mainInfo, crashed->currentTask, crashed->finished);
			if (recoveredTask) {
				list_add_back(recoverableTasks, recoveredTask);
			}
		}

		// Reroute future tasks
		reRouteTasks(mainInfo, crashed, recoverableTasks);
		
		// Debug stuff:
		printAllThreadAssignments(mainInfo->thConfig);
		printAll(mainInfo, false);

		// Alert the main thread that things have happened.
		pthread_cond_signal(mainInfo->signalCondition);
	pthread_mutex_unlock(mainInfo->usingTaskQueue);
	time_stopReconfig(mainInfo->diagnostics->time);
}
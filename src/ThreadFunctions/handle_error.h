/*
 * Copyright (C) 2020 Wouter Loeve <wouterloeve0@gmail.com>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HANDLE_ERROR_H
#define HANDLE_ERROR_H

#include "../Utils/global_structs.h"

void handleCrashError(MainStruct *mainInfo, ThreadStruct *crashed, CommunicationThread *commInfo);

/*
 * Invalidate the tasks in the given task queue.
 */
list_linked *invalidateTasks(MainStruct *mainInfo, list_linked *tasks, ThreadStruct *crashed);

/*
 * Invalidate the given task by setting the output struct to an error state.
 * When checkpoint restart is active, tasks will simply be added to the new component.
 */
TaskStruct *invalidateTask(MainStruct *mainInfo, TaskStruct *task, ReadyComponents *finished);

#endif
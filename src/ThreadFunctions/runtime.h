/*
 * Copyright (C) 2020 Wouter Loeve <wouterloeve0@gmail.com>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef RUNTIME_H
#define RUNTIME_H

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include "../Utils/global_structs.h"
#include "../Datastructures/taskgraph.h"
#include "../generated/generated.h"
#include "../Utils/arrays.h"

#include "init.h"



/*
 * Computation function of a thread.
 * Each thread will receive a threadstruct 
	with the relevant information for that specific thread.
 */
void *compThread(void * arg);


/*
 * Mainthread loop.
 */
void *mainThread(void *mainInfo);

/*
 * Loops trough the threads to see if any of them are done.
 * Will return false if none are done
 * Will return true if any is done
 */
bool checkThreadOutputs(MainStruct *mainInfo, bool onlyRoot);

/*
 * Puts the ready successors of a component in the ready struct.
 */
void checkSuccessorReady(GraphStruct *component, ReadyComponents *ready);

void checkPredecessorReady(GraphStruct *component, ReadyComponents *ready);

void addComponentToReady(GraphStruct *curGS, ReadyComponents *ready);

void addToTaskQueues(GraphStruct *node, MainStruct *mainInfo);
void addToTaskQueue(ThreadStruct *thread, TaskStruct *task);

void addToFinished(ReadyComponents *finished, int componentId, Diagnostics *diag);
#endif /* RUNTIME_H */
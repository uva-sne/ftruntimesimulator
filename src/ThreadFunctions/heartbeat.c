/*
 * Copyright (C) 2020 Wouter Loeve <wouterloeve0@gmail.com>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "heartbeat.h"

#include <math.h>
#include <unistd.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>

#include "../Utils/global_structs.h"
#include "../Utils/pthreads.h"
#include "../Utils/cleanup.h"
#include "handle_error.h"

/*
 * The worker heartbeat thread periodically resets the variable incremented by the checker thread, this is followed by a sleep.  
 */
void *commThread(void * arg) {
	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
	CommunicationThread *commInfo = (CommunicationThread *) arg;
	char *loc = calloc(1, sizeof(char) * (100));
	sprintf(loc, "Communication Thread (buddy: %i) id: %i", commInfo->buddyId, commInfo->threadId);
	DEBUG(loc, "Starting");
	
	while (true) {
		usleep(commInfo->sleepTime);

		int e = pthread_mutex_lock(commInfo->timeLock);
		handleMutexError(commInfo->timeLock, e);
		pthread_cleanup_push(mutex_cleanup_handler, commInfo->timeLock);

		commInfo->count = 0;
		clock_gettime(CLOCK_MONOTONIC_RAW, commInfo->curTime);
		pthread_cleanup_pop(1);
	}
}

/*
 * Main heartbeat thread function.
 * The checker does this by periodically looping trough the threads, 
 * 		incrementing a counter in memory shared with each heartbeat thread, followed by a sleep. 
 * After the increment of the counter, the heartbeat checker checks whether the counter is higher than a specified threshold, 
 * 		if it is higher, the worker heartbeat thread (and by extension the hardware component) is deemed unresponsive, 
 * 		i.e. it has crashed or is hanging.
 */
void *mainCommThread(void * arg) {
	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

	MainStruct *mainInfo = (MainStruct *) arg;
	CommunicationThread *commInfo = NULL;

	char *loc = calloc(1, sizeof(char) * (100));
	char *dbg = calloc(1, sizeof(char) * (100));
	sprintf(loc, "Master Communication Thread");
	DEBUG(loc, "Starting Communication Thread");

	struct timespec *curTime = malloc(sizeof(struct timespec));
	assert(curTime);

	sleep(2);

	while (true) {
		/*
		 * Config mainthreads is the number of computation units.
		 * mainInfo->numThreads is the number of actual threads, 
		 * 	including master and communication threads.
		 */
		for (int i = 0; i < mainInfo->config->numThreads; i++) {
			commInfo = mainInfo->commInfos[i];
			if (commInfo->crashed) {
				continue;
			}
			int e = pthread_mutex_lock(commInfo->timeLock);
			handleMutexError(commInfo->timeLock, e);
			pthread_cleanup_push(mutex_cleanup_handler, commInfo->timeLock);

			commInfo->count++;

			if (commInfo->count == mainInfo->config->heartbeatTries) {
				sprintf(dbg, "Communication thread irresponsive - id: %i, buddyId: %i", commInfo->threadId, commInfo->buddyId);
				DEBUG(loc, dbg);
				handleCrashError(mainInfo, mainInfo->threadInfos[commInfo->buddyId], commInfo);
				DEBUG(loc, "Handled error successfully");
			}
			pthread_cleanup_pop(1);
		}
		usleep(mainInfo->config->commTimeOut);
	}
}
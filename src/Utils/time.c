/*
 * Copyright (C) 2020 Wouter Loeve <wouterloeve0@gmail.com>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "time.h"

#include "stdlib.h"
#include "../Utils/pthreads.h"
#include <assert.h>

/*
 * Contains utility function for tracking time spent in simulation.
 * Each action that needs to be tracked needs to be defined seperately in a struct which can be found in time.h.
 * Start and stop functions for these actions can be found here.
 */

struct timespec *time_alloc_timespec() {
    struct timespec *t = malloc(sizeof(struct timespec));
    assert(t);
    return t;
}

threadTimeStruct *time_initTimeThread() {
    threadTimeStruct *t = malloc(sizeof(threadTimeStruct));
    assert(t);
    t->primaryBackup = 0;
    t->checkpointRestart = 0;
    t->total = 0;
    t->waiting = 0;
    t->waitingThread = 0;
    t->reconfig = 0;
    t->isUsingPrimaryBackup = false;
    t->isUsingCheckpoint = false;
    // t->wastedTimeCheckpoint = 0;

    t->startCheckpoint = time_alloc_timespec();
    t->startPrimaryBackup = time_alloc_timespec();
    t->startTotal = time_alloc_timespec();
    t->startGeneral = time_alloc_timespec();
    t->stopTemp = time_alloc_timespec();
    t->startReconfig = time_alloc_timespec();
    t->stopReconfig = time_alloc_timespec();
    t->lock = createPthreadMutex();
    return t;
}

void time_startTime(struct timespec *start) {
    clock_gettime(CLOCK_MONOTONIC, start);
}

double time_addTime(struct timespec *start, struct timespec *finish) {
    double elapsed = (finish->tv_sec - start->tv_sec);
    elapsed += (finish->tv_nsec - start->tv_nsec) / 1000000000.0;
    return elapsed;
}

double time_endTime(struct timespec *start, struct timespec *finish) {
    clock_gettime(CLOCK_MONOTONIC, finish);
    double elapsed = time_addTime(start, finish);
    return elapsed;
}

threadTimeStruct *time_addTimeStructs(threadTimeStruct * mainTime, threadTimeStruct *threadTime) {
    assert(mainTime != threadTime);
    mainTime->total += threadTime->total;
    mainTime->checkpointRestart += threadTime->checkpointRestart;
    mainTime->primaryBackup += threadTime->primaryBackup;
    mainTime->waitingThread += threadTime->waitingThread;

    return mainTime;
}

void time_stopWaitingThread(threadTimeStruct *threadTime) {
    threadTime->waitingThread += time_endTime(threadTime->startGeneral, threadTime->stopTemp);
}

void time_stopWaiting(threadTimeStruct *threadTime) {
    threadTime->waiting += time_endTime(threadTime->startGeneral, threadTime->stopTemp);
}

void time_startPrimaryBackup(threadTimeStruct *threadTime) {
    threadTime->isUsingPrimaryBackup = true;
    time_startTime(threadTime->startPrimaryBackup);
}

void time_stopPrimaryBackup(threadTimeStruct *threadTime) {
    if (threadTime->isUsingPrimaryBackup) {
        threadTime->primaryBackup += time_endTime(threadTime->startPrimaryBackup, threadTime->stopTemp);
    }
    threadTime->isUsingPrimaryBackup = false;
}

void time_startCheckpoint(threadTimeStruct *threadTime) {
    threadTime->isUsingCheckpoint = true;
    time_startTime(threadTime->startCheckpoint);
}

void time_stopCheckpointRestart(threadTimeStruct *threadTime) {
    if (threadTime->isUsingCheckpoint) {
        threadTime->checkpointRestart += time_endTime(threadTime->startCheckpoint, threadTime->stopTemp);
    }
    threadTime->isUsingCheckpoint = false;
}

void time_stopReconfig(threadTimeStruct *threadTime) {
    threadTime->reconfig += time_endTime(threadTime->startReconfig, threadTime->stopReconfig);
}

void time_startReconfig(threadTimeStruct *threadTime) {
    time_startTime(threadTime->startReconfig);
}

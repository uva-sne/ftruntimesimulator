/*
 * Copyright (C) 2020 Wouter Loeve <wouterloeve0@gmail.com>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "cleanup.h"

#include "./print.h"


void free_ThreadStruct(ThreadStruct *info) {
    // TODO
}

void free_MainStruct(MainStruct *mainInfo) {
    // TODO
}

/*
 * Frees and stops timers of a thread.
 */
void thread_cleanup(void *arg) {
    ThreadStruct *t = (ThreadStruct *) arg;
    t->time->total += time_endTime(t->time->startTotal, t->time->stopTemp);
    free_ThreadStruct(t);

    time_stopPrimaryBackup(t->time);
    time_stopCheckpointRestart(t->time);
}

/* 
 * Frees and stops timers of the main thread, cleans up all threads which are not cleanup already.
 */
void general_cleanup(MainStruct *mainInfo) {
    ThreadStruct *cur = NULL;
    threadTimeStruct *curTimeStruct = mainInfo->diagnostics->time;
    mainInfo->diagnostics->time->total += time_endTime(mainInfo->diagnostics->time->startTotal, mainInfo->diagnostics->time->stopTemp);

    pthread_cancel(*mainInfo->tids[mainInfo->numThreads - 1]);
    for (int i = 0; i < mainInfo->config->numThreads; i++) {
        cur = mainInfo->threadInfos[i];
        // Cleanup already occured
        if (!cur->crashed) {
            pthread_cancel(*mainInfo->tids[cur->buddy_id]);
            pthread_cancel(*mainInfo->tids[cur->thread_id]);
            // thread_cleanup(cur);
        }
    }
    for (int i = 0; i < mainInfo->config->numThreads; i++) {
        cur = mainInfo->threadInfos[i];
        if (!cur->crashed) {
            pthread_join(*mainInfo->tids[cur->thread_id], NULL);

        }
        curTimeStruct = time_addTimeStructs(curTimeStruct, cur->time);
    }
    // printTimes(stderr, curTimeStruct);
    mainInfo->diagnostics->time = curTimeStruct;

    printAll(mainInfo, (mainInfo->diagnostics->logFileLoc != NULL));
    free_MainStruct(mainInfo);
    exit(0);
}

/*
 * POSIX threads can utilise a cleanup handler to deal with crashes.
 * This function is such a cleanup handler because it unlocks the mutex.
 * This alleviates a problem that exists when a thread is crashed while it own a mutex.
 */
void mutex_cleanup_handler(void *arg) {
    pthread_mutex_t *m = (pthread_mutex_t *) arg;
    pthread_mutex_unlock(m);
}
/*
 * Copyright (C) 2020 Wouter Loeve <wouterloeve0@gmail.com>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef PRINT_H
#define PRINT_H

#include "global_structs.h"

void printHelp();

void printConfig(SimulationConfig *config);

void printThreadConfig(ThreadConfig *config);

void printDiagnostics(FILE *f, Diagnostics *d, task_graph *taskgraph);

void printAll(MainStruct *mainInfo, bool writeToFile);

void printTimes(FILE * file, threadTimeStruct *t);

#endif /* PRINT_H */
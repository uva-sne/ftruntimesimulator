/*
 * Copyright (C) 2020 Wouter Loeve <wouterloeve0@gmail.com>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef LOG_H
#define LOG_H

#include <errno.h>
#include <pthread.h>

#include "../main.h"

/*
 * Provides a set of functions to log and handle errors.
 * Names are capitalised to not meddle with the normal variable namespace.
 */

/* 
 * Prints a warning to standard error.
 */
void WARNING(char *loc, char *msg);

/* Warning which frees the msg
 */
void FREEWARNING(char *loc, char *msg);

/*
 * Prints an error message and exits the simulation.
 */
void ERROR(char *loc, char *msg);

/*
 * Prints a debug message if debugging is enabled (can be done using the configuration file).
 */
void DEBUG(char *loc, char *msg);

/*
 * Handles pthread initialition error codes.
 */
void PTHREAD_INIT_ERROR(int errormsg);

#endif /* LOG_H */
/*
 * Copyright (C) 2020 Wouter Loeve <wouterloeve0@gmail.com>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "arrays.h"
#include <stdio.h>

/*
 * Removes duplicates from an integer array with the given size.
 */
void arrayRemoveDups(int *arr, int size) {
    for(int i=0; i<size; i++) {
        for(int j=i+1; j<size; j++) {
            /* If any duplicate found */
            if(arr[i] == arr[j]) {
                /* Delete the current duplicate element */
                for(int k=j; k<size; k++) {
                    if (k+1 != size) {
                        arr[k] = arr[k + 1];
                    }
                }

                /* Decrement size after removing duplicate element */
                size--;

                /* If shifting of elements occur then don't increment j */
                j--;
            }
        }
    }
}

/*
 * Does the array have a member equal to the given item?
 */
bool hasMember(int *arr, int size, int item) {
    for (int i = 0; i < size; i++) {
        if (item == arr[i]) {
            return true;
        }
    }

    return false;
}
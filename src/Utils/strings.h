#ifndef STRINGS_H
#define STRINGS_H

#include "stdlib.h"
#include <string.h>

char *trimwhitespace(char *str);

char *strcpy_alloc(char *dest, char *src);

int string_to_int(char *value);

u_int64_t string_to_uint64(char *value);

#endif /* STRINGS_H */
/*
 * Copyright (C) 2020 Wouter Loeve <wouterloeve0@gmail.com>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef TIME_H
#define TIME_H

#include <time.h>
#include <pthread.h>
#include <stdbool.h>

/*
 * Assumption is that you do not count FT methods like primarybackup and checkpoint restart at the same time.
 */
typedef struct {
    double reconfig;
    double primaryBackup;
    double checkpointRestart;
    double total;
    double waiting;
    double waitingThread;
    double copyTime;
    // double wastedTimeCheckpoint;
    bool isUsingPrimaryBackup;
    bool isUsingCheckpoint;
    struct timespec *startPrimaryBackup;
    struct timespec *startCheckpoint;
    struct timespec *startReconfig;
    struct timespec *stopReconfig;
    struct timespec *startTotal;
    struct timespec *startGeneral;
    struct timespec *stopTemp;
    struct timespec *startCopy;
    pthread_mutex_t *lock;
} threadTimeStruct;

void time_startTime(struct timespec *start);

double time_endTime(struct timespec *start, struct timespec *finish);

double time_addTime(struct timespec *start, struct timespec *finish);

threadTimeStruct *time_addTimeStructs(threadTimeStruct * mainTime, threadTimeStruct *threadTime);

threadTimeStruct *time_initTimeThread();


/*
 * Stop thread timer functions:
 */
void time_stopCheckpointRestart(threadTimeStruct *threadTime);

void time_stopPrimaryBackup(threadTimeStruct *threadTime);

void time_stopWaiting(threadTimeStruct *threadTime);
#endif

void time_stopWaitingThread(threadTimeStruct *threadTime);

void time_startReconfig(threadTimeStruct *threadTime);

void time_stopReconfig(threadTimeStruct *threadTime);

void time_stopCheckpointRestart(threadTimeStruct *threadTime);

void time_startCheckpoint(threadTimeStruct *threadTime);

void time_startPrimaryBackup(threadTimeStruct *threadTime);

// void time_startCopy(threadTimeStruct *threadTime);

// void time_stopCopy(threadTimeSTruct *threadTime);
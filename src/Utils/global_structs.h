/*
 * Copyright (C) 2020 Wouter Loeve <wouterloeve0@gmail.com>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef GLOBAL_STRUCTS_H
#define GLOBAL_STRUCTS_H

#include <pthread.h>
#include <semaphore.h>
#include "../Datastructures/linkedlist.h"
#include "../Datastructures/circularbuffer.h"
#include "../Datastructures/profile.h"
#include <sys/time.h>
#include <stdbool.h>
#include <stdio.h>
#include "./time.h"

typedef struct {
    char *configfile;
    char *thConfigFile;
    bool printConfig;
    char *threadComponentfile;
    int numThreads;
    unsigned int commSleepTime;
    unsigned int commTimeOut;
    int heartbeatTries;
    int heartbeatCheckerPrio;
    int heartbeatWorkerPrio;
    char *diagnosticsOutput;
    bool standbyEarlyTaskCompletion;
} SimulationConfig;

typedef struct {
    char *class;
    int threadId;
    list_linked *componentNames;
} ThreadConfigItem;

typedef struct {
    ThreadConfigItem **items;
    int size;
} ThreadConfig;

typedef struct {
    char *componentName;
    int componentId;
    int taskId;
    bool done;
    // Is null when source component
    void *inputData;
    // Is null when sink component
    void *outputData;

    /*
     * Shared variables between copies.
     * Access with node mutex lock!!
     */
    // Number of times it is delivered
    int *delivered;
    // Number of replicas crashed.
    int *crashed;
    bool *checkpointed;
} TaskStruct;

typedef struct {
    char *componentName;
    int componentId;
    // type: EdgeStruct*
    list_linked *prevEdges;
    // Can be used to find the correct buffer
    char **inport;
    int numInports;

    // type: EdgeStruct*
    list_linked *nextEdges;
    // Outports are available in the 
    // nextedges itself as an inport since there is always a single inport.

    // If the component is in queue/computing it should not be added to the queue again since we don't have a guarantee on which output will be delivered first.
    bool inQueue;
    bool inReady;

    Profile *profile;

    // Id(s) of the thread the component should run on.
    // Multiple iff fault-tolerance method is used.
    int *threadId;
    int threadIdSize;
    int threadIdIndex;

    pthread_mutex_t *nodeMutex;
} GraphStruct;

struct edgeInfo;

/*
 * Inport to the component
 */
typedef struct {
    int multiplicity;
    GraphStruct *graph;
    circ_buf *buffer;
    circ_buf *checkpointBuffer;
    char *portname;

    // Struct to carry multiplicity in.
    void **currdata;

    // Handle back to edge.
    struct edgeInfo *edge;
} OutportStruct;

struct edgeInfo {
    // Source port
    int outputIndex;
    // Inport of the edge
    char *inport;
    int outputSize;
    
    GraphStruct *input;

    pthread_mutex_t *mutex;

    OutportStruct **outports;
};

typedef struct edgeInfo EdgeStruct;

typedef struct {
    // GraphStruct linked list
    list_linked *roots;
    int compsIndex;
    int numComps;
    int numEdges;
    GraphStruct **taskList;
} task_graph;

typedef struct {
	int *componentIds;
	int size;
	int readyIndex;
    pthread_mutex_t *readyLock;
} ReadyComponents;

struct minfo;

typedef struct {
	SimulationConfig *config;
    char *class;
	int thread_id;
    int buddy_id;
    sem_t *signalSemaphore;

    pthread_cond_t *inputCondition;
    pthread_mutex_t *outputLock; 
    
    void *outputData;
    TaskStruct *currentTask;
    /* 
     * When signaled the main thread will 
     * go trough the threads to collect outputs.
     */
    pthread_cond_t *signalCondition;

    list_linked *taskQueue;
    pthread_mutex_t *taskQueueLock;
    // For cleanup handler
    bool hasTaskQueueLock;
    sem_t *threadSemaphore;

    ReadyComponents *finished;
    struct minfo *mainInfo;
    bool crashed;

    threadTimeStruct* time;
} ThreadStruct;

typedef struct {
    int threadId;
    int buddyId;
    int count;
    unsigned int sleepTime;
    struct timespec *curTime;
    pthread_mutex_t *timeLock;
    bool crashed;
} CommunicationThread;

// Todo add mutex, is only updated during reconfiguring for now so it works for now.
typedef struct {
    int *crashedComponents;
    int numCrashed;
    int numCheckpoint;
    int numPrimaryBackup;
    int *directlyCrashedComps;
    int *firedComponentFreqs;
    int *errorTokenComponentFreqs;
    int *savedComponents;
    FILE *logFile;
    char *logFileLoc;
    threadTimeStruct *time;
} Diagnostics;

struct minfo {
    SimulationConfig *config;
    ThreadConfig *thConfig;

    ThreadStruct **threadInfos;
    CommunicationThread **commInfos;

    pthread_mutex_t *threadInfosLock;
    pthread_cond_t *signalCondition;
    pthread_cond_t *inputCondition;
    pthread_t **tids;

    /* Locked when the main threads interacts with the taskqueues, 
     * this interaction and error handling cannot occur at the same time.
     */
    pthread_mutex_t *usingTaskQueue;

    // Pointer to the error token.
    // If you take this out of a buffer a predecessor task has failed
    void *errorToken;

    /* Total number of threads including 
     *  mainthread and communications threads.
     * For the number of computation threads check config.
     */
    int numThreads;

    // Application taskgraph
    task_graph *taskGraph;

    ReadyComponents *ready;
    ReadyComponents *finished;

    Diagnostics *diagnostics;
};
typedef struct minfo MainStruct;


#endif /* GLOBAL_STRUCTS_H */
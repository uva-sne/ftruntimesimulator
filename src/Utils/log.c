/*
 * Copyright (C) 2020 Wouter Loeve <wouterloeve0@gmail.com>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "log.h"

#include <stdio.h>
#include <stdlib.h>
#include "./cleanup.h"

/*
 * Prints a warning with a location and a message.
 * Warning doesn't exit.
 */
void WARNING(char *loc, char *msg) {
	fprintf(stderr, "WARNING: in %s - %s msg\n", loc, msg);
}

/*
 * Warning but it frees the message.
 */
void FREEWARNING(char *loc, char *msg) {
	WARNING(loc, msg);
	free(msg);
}

/*
 * Warning with exit.
 */
void ERROR(char *loc, char *msg) {
	fprintf(stderr, "ERROR: in %s - %s\n", loc, msg);
	exit(1);
}

/*
 * Debug print, only executes if debug is set to true in configuration.
 */
void DEBUG(char *loc, char *msg) {
	if (debug) {
		fprintf(stderr, "DEBUG: %s - %s\n", loc, msg);
	}
}

/*
 * Handles poxis threads initialisation errors.
 */
void PTHREAD_INIT_ERROR(int errormsg) {
	if (errormsg == EAGAIN) {
		ERROR("pthread func", "System lacks necessary resources");
	} else if (errormsg == ENOMEM) {
		ERROR("pthread func", "Insufficient memory");
	} else if (errormsg == EPERM) {
		ERROR("pthread func", "Insufficient permission");
	} else if (errormsg == EINVAL) {
		ERROR("pthread func", "Attribute is invalid");
	}
}
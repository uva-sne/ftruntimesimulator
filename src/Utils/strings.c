#include "strings.h"

#include <string.h>
#include <assert.h>
#include <ctype.h>
#include "log.h"
#include "stdint.h"

// Note: This function returns a pointer to a substring of the original string.
// If the given string was allocated dynamically, the caller must not overwrite
// that pointer with the returned value, since the original pointer must be
// deallocated using the same allocator with which it was allocated.  The return
// value must NOT be deallocated using free() etc.
// src: https://stackoverflow.com/questions/122616/how-do-i-trim-leading-trailing-whitespace-in-a-standard-way
char *trimwhitespace(char *str)
{
	char *end;

	// Trim leading space
	while(isspace((unsigned char)*str)) str++;

	if(*str == 0)  // All spaces?
		return str;

	// Trim trailing space
	end = str + strlen(str) - 1;
	while(end > str && isspace((unsigned char)*end)) end--;

	// Write new null terminator character
	end[1] = '\0';

	return str;
}

/*
 * Strcpy with allocation of the new string.
 */
char* strcpy_alloc(char *dest, char *src) {
	dest = malloc(sizeof(char) * (strlen(src)+1));
	assert(dest);
	strcpy(dest, src);
	return dest;
}

/*
 * Converts a string to an int.
 */
int string_to_int(char *value) {
	return (int) strtol(value, (char **)NULL, 10);
}

/*
 * Converts a string to a 64-bit int.
 */
uint64_t string_to_uint64(char *value) {
	return (uint64_t) strtol(value, (char **)NULL, 10);
}
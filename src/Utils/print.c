/*
 * Copyright (C) 2020 Wouter Loeve <wouterloeve0@gmail.com>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
 * Print functions for several data structures.
 */

#include "print.h"
#include <string.h>

/*
 * Prints the help of the command line arguments.
 */
void printHelp() {
    fprintf(stderr,"----- Help -----\n");
    fprintf(stderr,"-c \t configfile location\n");
    fprintf(stderr,"-t \t thread config file location\n");
    fprintf(stderr,"-p \t print config\n");
    fprintf(stderr,"-o \t list of thread crash orders\n");
    fprintf(stderr,"-s \t list of sleep lengths between crashes\n");
}

/*
 * Prints the configuration for this simulation.
 */
void printConfig(SimulationConfig *config) {
    fprintf(stderr,"----Config----\n");
    fprintf(stderr,"numThreads: %d\n", config->numThreads);
    fprintf(stderr,"debug: %d\n", debug);
    fprintf(stderr,"sleepTime: %i\n", config->commSleepTime);
    fprintf(stderr,"timeOut: %i\n", config->commTimeOut);
    fprintf(stderr,"heartbeatTries: %i\n", config->heartbeatTries);
    fprintf(stderr,"heartbeatWorkerPrio: %i\n", config->heartbeatWorkerPrio);
    fprintf(stderr,"heartbeatCheckerPrio: %i\n", config->heartbeatCheckerPrio);
    fprintf(stderr, "standbyEarlyTaskCompletion: %i\n", config->standbyEarlyTaskCompletion);
    fprintf(stderr,"\n");
}

/*
 * Prints the thread configuration (i.e., which components are to be executed by which threads).
 */
void printThreadConfig(ThreadConfig *config) {
    fprintf(stderr,"--ThreadConfig--\n");
    ThreadConfigItem *cur = NULL;
    list_node *curNode = NULL;
    for (int i = 0; i < config->size; i++) {
        cur = config->items[i];
        fprintf(stderr,"%s %i = ", cur->class, cur->threadId);

		if (cur->componentNames->size == 0) {
			fprintf(stderr,"None\n");
		}
        curNode = list_get_first(cur->componentNames);
        for (int j = 0; j < cur->componentNames->size; j++) {
            char *compName = list_get_data(curNode);

            if (strcmp(compName, "") != 0) {
                fprintf(stderr,"%s", compName);
                if (list_get_next(curNode) != NULL) {
                    fprintf(stderr,", ");
                }   
            }
            
            curNode = list_get_next(curNode);
        }
        fprintf(stderr,"\n");
    }   
    fprintf(stderr,"\n");
}

/*
 * Prints the current diagnostics of the simulations.
 * For example number of executions, timing information etc.
 */
void printDiagnostics(FILE *f, Diagnostics *d, task_graph *taskgraph) {
	fprintf(f,"--Diagnostics--\n");

    fprintf(f,"--CrashedStats--\n");
	for (int i = 0; i < d->numCrashed; i++) {
		fprintf(f,"\tThread with id: %i crashed\n", d->crashedComponents[i]);
	}

	fprintf(f,"Checkpoint/restart: %i times\n", d->numCheckpoint);
	fprintf(f,"Primary-backup: %i times\n", d->numPrimaryBackup);
    fprintf(f,"--ErrorTokenFreqs--\n");
    for (int i = 0; i < taskgraph->numComps; i++) {
        GraphStruct *cur = taskgraph->taskList[i];
        fprintf(f,"\tErrorToken %s: %i times\n", cur->componentName, d->errorTokenComponentFreqs[i]);
    }
    fprintf(f,"\n");
    fprintf(f,"--ComponentFireFreqs--\n");
    for (int i = 0; i < taskgraph->numComps; i++) {
        GraphStruct *cur = taskgraph->taskList[i];
        fprintf(f,"\tFired %s: %i times\n", cur->componentName, d->firedComponentFreqs[i]);
    }

    fprintf(f,"--savedComponents--\n");
    for (int i = 0; i < taskgraph->numComps; i++) {
        GraphStruct *cur = taskgraph->taskList[i];
        fprintf(f,"\tSaved %s: %i times\n", cur->componentName, d->savedComponents[i]);
    }

    fprintf(f,"--directlyCrashedComps--\n");
    for (int i = 0; i < taskgraph->numComps; i++) {
        GraphStruct *cur = taskgraph->taskList[i];
        fprintf(f,"\tComponent %s crased: %i times\n", cur->componentName, d->directlyCrashedComps[i]);
    }
}

/*
 * Print the timing information of a thread.
 */
void printTimes(FILE * file, threadTimeStruct *t) {
    fprintf(file, "--TimeStruct--\n");
    fprintf(file, "Primarybackup: %f\n", t->primaryBackup);
    fprintf(file, "Checkpointrestart: %f\n", t->checkpointRestart);
    fprintf(file, "total: %f \n", t->total);
    fprintf(file, "waitingMain: %f \n", t->waiting);
    fprintf(file, "waitingThread: %f \n", t->waitingThread);
    fprintf(file, "reconfig: %f \n", t->reconfig);
    fprintf(file, "\n");
}

//TODO export diagnostics to a file.
void exportDiagnostics() {

}

/*
 * Print all relevant structs (no help)
 */
void printAll(MainStruct *mainInfo, bool writeToFile) {
    FILE *output = stderr;
    if (writeToFile) {
        output = mainInfo->diagnostics->logFile;
    }

    printConfig(mainInfo->config);
    printThreadConfig(mainInfo->thConfig);
    printDiagnostics(output, mainInfo->diagnostics, mainInfo->taskGraph);
    printTimes(output, mainInfo->diagnostics->time);
    fprintf(output, "--------------------\n");
}

/*
 * Copyright (C) 2020 Wouter Loeve <wouterloeve0@gmail.com>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "pthreads.h"

#include <stdlib.h>
#include <assert.h>

#include "log.h"

/*
 * Pthread helper functions.
 */

/*
 * Allocate a pthread condition variable and returns a pointer to it.
 */
pthread_cond_t *createPthreadCond() {
	pthread_cond_t *cond = malloc(sizeof(pthread_cond_t));
	PTHREAD_INIT_ERROR(pthread_cond_init(cond, NULL));
	return cond;
}

/*
 * Set the scheduling options of the thread to round robin thread.
 */
pthread_attr_t *createRoundRobinAttr() {
	pthread_attr_t *my_attr = malloc(sizeof(pthread_attr_t));
	pthread_attr_init(my_attr);
	handleSchedPolicyError(pthread_attr_setschedpolicy(my_attr, SCHED_RR));

	return my_attr;
}

/*
 * Allocates a pthread mutex lock and returns a pointer to it
 */
pthread_mutex_t *createPthreadMutex() {
	pthread_mutex_t *lock = malloc(sizeof(pthread_mutex_t));
	pthread_mutexattr_t *attr = malloc(sizeof(pthread_mutexattr_t));
	pthread_mutexattr_init(attr);
	// pthread_mutexattr_setrobust(attr, PTHREAD_MUTEX_ROBUST);
	
	PTHREAD_INIT_ERROR(pthread_mutex_init(lock, NULL));
	return lock;
}

/*
 * Handles mutex errors.
 * TODO handle other errors.
 * Specifically the EOWNERDEAD which happens when a mutex is attempted to be locked but the owning thread is cancelled.
 * First I tried to use robust mutex, that didn't work, so I used cleanup functions instead.
 */
bool handleMutexError(pthread_mutex_t *mutex, int errorCode) {
	if (errorCode == EOWNERDEAD) {
		int s = pthread_mutex_consistent(mutex);
			
		if (s == 0) {
			return true;
		} else {
			return false;
		}
	}
	return true;
}

/*
 * Handle errors in setting the scheduling pollicy.
 */
bool handleSchedPolicyError(int returnVal) {
	if (returnVal == EINVAL) {
		ERROR("pthreads.c", "Return value for scheduling policy is invalid");
	} else if (returnVal == ENOTSUP) {
		ERROR("pthreads.c", "Attempt was made to set the attribute to an unsupported value");
		return false;
	}
	return true;
}

/*
 * Allocates a semaphore and returns a pointer to it.
 */
sem_t *createSemaphore(int val) {
	sem_t *s = malloc(sizeof(sem_t));
	assert(sem_init(s, 0, val) == 0);
	return s;
}
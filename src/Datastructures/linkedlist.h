/*
 * Copyright (C) 2020 Wouter Loeve <wouterloeve0@gmail.com>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef LINKED_LIST_H
#define LINKED_LIST_H

#include <stdlib.h>
#include "../Utils/log.h"
#include <assert.h>

struct linkedlist_node {
	void *data;
	struct linkedlist_node *next;
};

typedef struct linkedlist_node list_node;

typedef struct {
	list_node *first;
	int size;
} list_linked;

list_linked *list_init();

int list_cleanup(list_linked* list);

int list_add_back(list_linked *list, void *data);

list_node *list_pop_first(list_linked *list);

list_node *list_pop_last(list_linked *list);

int list_free_node(list_node *n);

/*
 * retrieves the nth element from the list
 */
list_node *list_get_n(list_linked *list, int n);

list_node *list_get_last(list_linked *list);

list_node *list_get_first(list_linked *list);

list_node *list_get_next(list_node *node);

void *list_get_data(list_node *n);

#endif /* LINKED_LIST */
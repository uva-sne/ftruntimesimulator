/*
 * Copyright (C) 2020 Wouter Loeve <wouterloeve0@gmail.com>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef PROFILE_H
#define PROFILE_H

#include "./linkedlist.h"
#include "stdint.h"
#include <stdbool.h>

typedef struct {
	bool shrinking;
	int frequency;
} CheckPointProfile;

enum sync_enum { cold, hot };

typedef struct {
	int replicas;
	enum sync_enum sync;
} StandbyProfile;

typedef struct {
	int replicas;
	char *versionName;
} VersionItem;

typedef struct {
	// Type: VersionItem.
	list_linked *versions;
} nVersionProfile;

enum waitingStart_enum { single, majority };

typedef struct {
	int replicas;
	int votingReplicas;
	int waitingTime;
	enum waitingStart_enum waitingStart;
	// Todo implement in xtext.
	bool waitingJoin;
} nModularProfile;

typedef struct {
	nModularProfile *nModular;
	nVersionProfile *nVersion;
	StandbyProfile *standby;
	CheckPointProfile *checkpoint;
	uint64_t deadline;
	uint64_t period;
	char *implementationName;
} Profile;

/*
 * -------------------- Setters ------------------------------
 */

/*
 * Initialise Profile struct
 */
Profile *profile_init();

/*
 * Add normal option to profile
 */
Profile *profile_add(Profile *p, char *opt, char *val);

/*
 * Initialise nMmodular struct
 * Needs to be called on Profile before profile_nModular_add can be called.
 */
Profile *profile_nModular_init(Profile *p);

/*
 * Initialise nVersion struct
 * Needs to be called on Profile before profile_nVersion_add can be called.
 */
Profile *profile_nVersion_init(Profile *p);

/*
 * Initialise standby struct
 * Needs to be called on Profile before profile_standby_add can be called.
 */
Profile *profile_standby_init(Profile *p);

/*
 * Initialise checkpoint struct
 * Needs to be called on Profile before profile_checkpoint_add can be called.
 */
Profile *profile_checkpoint_init(Profile *p);

Profile *profile_checkpoint_add(Profile *p, char *opt, char *val);

Profile *profile_standby_add(Profile *p, char *opt, char *val);

Profile *profile_nVersion_add(Profile *p, char *opt, char *val);

Profile *profile_nModular_add(Profile *p, char *opt, char *val);

/*
 * -------------------- Getters ------------------------------
 */

int profile_get_num_replicas(Profile *p);

bool profile_get_checkpoint(Profile *p);

bool profile_get_standby(Profile *p);
#endif /* PROFILE_H */

/*
 * Copyright (C) 2020 Wouter Loeve <wouterloeve0@gmail.com>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef CIRCULAR_BUFFER_H
#define CIRCULAR_BUFFER_H

#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>

#include "../Utils/log.h"

typedef struct {
	void **buffer;
	int head;
	int tail;
	int max;
	bool full;
} circ_buf;

circ_buf *circ_buf_init(int size);

int circ_buf_add(circ_buf *b, void *data);

void circ_buf_advance_pointer(circ_buf *b);

void circ_buf_retreat_pointer(circ_buf *b);

void *circ_buf_get(circ_buf *b);

bool circ_buf_full(circ_buf *b);

bool circ_buf_empty(circ_buf *b);

int circ_buf_size(circ_buf* b);

bool circ_buf_can_take(circ_buf *b, int i);

#endif /* CIRCULAR_BUFFER_H */

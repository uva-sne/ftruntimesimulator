/*
 * Copyright (C) 2020 Wouter Loeve <wouterloeve0@gmail.com>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "circularbuffer.h"

/*
 * Circular buffer with void pointers
 *      which does not accept new items when it is full.
 */

/*
 * Initialise circular buffer with given size.
 */
circ_buf *circ_buf_init(int size) {
    circ_buf *b = malloc(sizeof(circ_buf));
    assert(b);
    b->max = size;
    b->head = 0;
    b->tail = 0;
    b->full = false;

    void **buffer = malloc(sizeof(void*) * size);
    assert(buffer);
    b->buffer = buffer;

    return b;
}

/*
 * Adds a void pointer to the circular buffer.
 */
int circ_buf_add(circ_buf *b, void *data) {
    assert(b);
    assert(data);

    if (! b->full) {
        b->buffer[b->head] = data;
        circ_buf_advance_pointer(b);
        return 0;
    }

    return 1;
}

/*
 * Advances the buffer pointer and handles wrap-around
 */
void circ_buf_advance_pointer(circ_buf *b) {
	assert(b);

    // Should not happen anymore
	if (b->full) {
		b->tail = (b->tail + 1) % b->max;
	}

	b->head = (b->head + 1) % b->max;
	b->full = (b->head == b->tail);
}

/*
 * Lowers the pointer and handles wrap around
 */
void circ_buf_retreat_pointer(circ_buf *b) {
	assert(b);

	b->full = false;
	b->tail = (b->tail + 1) % b->max;
}

/*
 * Peek the top item of the buffer.
 */
void *circ_buf_get(circ_buf *b) {
    assert(b && b->buffer);
    void *data = NULL;

    if(!circ_buf_empty(b)) {
        data = b->buffer[b->tail];
        circ_buf_retreat_pointer(b);
    }

    return data;
}

/*
 * Returns a boolean indicating whether the circular buffer is empty.
 */
bool circ_buf_empty(circ_buf *b) {
	assert(b);

    return (!b->full && (b->head == b->tail));
}

/*
 * Returns a boolean indicating whether the circular buffer is empty.
 */
bool circ_buf_full(circ_buf *b) {
    assert(b);

    return b->full;
}

/*
 * Returns a boolean indicating whether the 
 *    circular buffer can hold i more items.
 */
bool circ_buf_can_take(circ_buf *b, int i) {
    assert(b);

    return (b->max - circ_buf_size(b) >= i);
}

/*
 * Frees the circular buffer
 * TODO Free tiems
 */
void circ_buf_free(circ_buf *b) {
    assert(b);
    free(b);
}

/*
 * Returns an integer representing the number of items the buffer has
 */
int circ_buf_size(circ_buf* b) {
	assert(b);

	size_t size = b->max;

	if(!b->full) {
		if(b->head >= b->tail) {
			size = (b->head - b->tail);
		}
		else {
			size = (b->max + b->head - b->tail);
		}
	}

	return size;
}
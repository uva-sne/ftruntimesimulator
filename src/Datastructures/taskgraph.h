/*
 * Copyright (C) 2020 Wouter Loeve <wouterloeve0@gmail.com>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef TASK_GRAPH_H
#define TASK_GRAPH_H

#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include <string.h>
#include "../Utils/strings.h"

#include "../Utils/global_structs.h"
#include "linkedlist.h"

/*
 * Initializes a task graph.
 */
task_graph *task_graph_init(int num_tasks);

void task_graph_print_GS(GraphStruct *gs);

void task_graph_print(task_graph *g);

/*
 * Creates a GraphStruct
 */
GraphStruct *task_graph_create_gs(char *name, int id, char** inports, int numInports);

/*
 * Creates an EdgeStruct
 * Meant for internal use only.
 */
EdgeStruct *task_graph_create_edge(char* outport, char *inport, int numOutports, GraphStruct *input, int multiplicit, GraphStruct *cur);

/*
 * Adds a new component with an edge to a graph.
 * Inport is the inport of the edge, so the outport of the first component.
 * Outport is the (first) out port of the edge, so the inport of the second component.
 * Likewise, numOutports references the number of outports of the edge.
 */
void task_graph_add_comp(task_graph *g, GraphStruct *prev, GraphStruct *cur, char *outport, int multiplicity, char *inport, int numOutports);

/*
 * Adds an edge between cur and prev, but cur already exists in the graph.
 */
void task_graph_add_edge(task_graph *g, GraphStruct *prev, GraphStruct *cur, char *outport, int multiplicity, char *inport, int numOutports);

/*
 * Adds a destination to an edge.
 */
void task_graph_add_edge_dest(EdgeStruct *edge, GraphStruct *dest);

/*
 * Adds a root task to the graph
 */
void task_graph_add_root(task_graph *g, GraphStruct *cur);

/*
 * Adds the given data to the edge corresponding with the specified inport linked to the specified node.
 */
void task_graph_add_to_buffer(GraphStruct *node, void *data, char *outport, void *(*copy_func)(void *), threadTimeStruct *time);

TaskStruct *task_graph_create_task(int componentId, char *componentName, void *inputData, void *outputData);

TaskStruct *task_graph_copy_task(TaskStruct *task, void *inputData, void *outputData);

/*
 * Returns whether a node/component has sufficient input tokens on its input port.
 * i.e., it returns true when it is ready to fire.
 */
bool task_graph_ready_to_fire(GraphStruct *node);

/*
 * Creates an outport struct.
 * WARNING: Graph not set here.
 */
OutportStruct *task_graph_create_os(EdgeStruct *edge, int multiplicity, char *portname, GraphStruct *cur);

void **task_graph_get_data_by_port(char *port, list_linked *edges, bool checkpoint);

OutportStruct *task_graph_get_OS_by_port(char *port, list_linked *edges);

int task_graph_get_num_roots(task_graph *g);

GraphStruct *task_graph_get_gs_by_id(task_graph *g, int id);

list_node *task_graph_get_roots(task_graph *g);

int task_graph_get_num_replicas(GraphStruct *gs);

/*
 * Replaces the id corresponding with curThreadId with newThread id.
 */ 
void task_graph_recover(GraphStruct *gs, int curThreadId, int newThreadId);

GraphStruct *task_graph_get_gs_by_name(task_graph *g , char *name);

/*
 * Adds the threadId to the threadId array.
 * Array is size one if no replicas-based FT method is used.
 */
void task_graph_set_thread_id(GraphStruct *gs, int threadId);

bool task_graph_outport_has_checkpoint(OutportStruct *os);

bool task_graph_is_root(GraphStruct *g);

#endif /* TASK_GRAPH_H */
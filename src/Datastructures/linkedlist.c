/*
 * Copyright (C) 2020 Wouter Loeve <wouterloeve0@gmail.com>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "./linkedlist.h"

/*
 * Singly linked list implementation.
 * TODO optimise to doubly linked list
 */

/*
 * Initialise a boundless linked list.
 */
list_linked *list_init() {
	list_linked *list = (list_linked*) malloc(sizeof(list_linked));
	assert(list);
	list->size = 0;
	list->first = NULL;

	return list;
}

/*
 * Free a node.
 */
int list_free_node(list_node *n) {
	free(n);
	return 0;
}

/*
 * Frees the entire list.
 */
int list_cleanup(list_linked* list) {
	assert(list);
	list_node *current = list->first;
	list_node *next = current->next;

	while (current) {
		next = current->next;
		free(current);
		current = next;
	}

	free(list);
	return 0;
}

/*
 * Creates a new node with the given data and 
 * 		adds it to the back of the linked list.
 */
int list_add_back(list_linked *list, void *data) {
	assert(list && data);
	
	list_node *n = malloc(sizeof(list_node));
	assert(n);
	n->data = data;
	n->next = NULL;

	if (list->size == 0) {
		list->first = n;
	} else {
		list_node *last = list_get_last(list);
		last->next = n;
	}

	list->size++;
	return 0;
}

/*
 * Return the successor item of the given node.
 */
list_node *list_get_next(list_node *node) {
	if (node->next) {
		return node->next;
	}
	return NULL;
}

/*
 * Removes the first item from the linked list and returns it.
 */
list_node *list_pop_first(list_linked *list) {
	assert(list);
	if (list->size == 0) {
		return NULL;
	}

	list_node * node = list->first;
	list->first = node->next;
	list->size--;

	return node;
}

/*
 * Removes the last item from the linked list and returns it.
 */
list_node *list_pop_last(list_linked *list) {
	assert(list);
	if (list->size == 0) {
		return NULL;
	}

	list_node * node = list_get_last(list);
	if (node != NULL) {
		return NULL;
	}

	list->first = node->next;
	list->size--;

	return node;
}

/*
 * Returns the n'th item in the linkedlist.
 */
list_node *list_get_n(list_linked *list, int n) {
	assert(list);
	if (list->size == 0 || list->size-1 < n) {
		return NULL;
	}

	int i = 0;
	list_node *current = list->first;
	while (current) {
		if (i == n) {
			return current;
		}
		i++;
		current = current->next;
	}

	return current;
}

/*
 * Returns the last item of the linkedlist.
 * TODO update administration to use last item pointer.
 */
list_node *list_get_last(list_linked *list) {
	assert(list);
	if (list->size == 0) {
		return NULL;
	}

	list_node *current = list_get_n(list, list->size-1);
	if (current == NULL) {
		return NULL;
	}

	return current;
}

/*
 * Returns the first item of the linkedlist.
 */
list_node *list_get_first(list_linked *list) {
	assert(list);
	if (list->size == 0) {
		return NULL;
	}
	return list->first;
}

/*
 * Returns a void pointer to the data field of a node.
 */
void *list_get_data(list_node *n) {
	assert(n);
	return n->data;
}
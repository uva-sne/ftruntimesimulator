/*
 * Copyright (C) 2020 Wouter Loeve <wouterloeve0@gmail.com>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "profile.h"

#include <stdlib.h>

#include "../Utils/log.h"
#include "../Utils/strings.h"

/*
 * Initialise a profile data structure and setts all options to NULL.
 */
Profile *profile_init() {
	Profile *p = malloc(sizeof(Profile));
	assert(p);
	p->checkpoint = NULL;
	p->nModular = NULL;
	p->standby = NULL;
	p->nVersion = NULL;
	return p;
}

/* 
 * Adds a normal (non-ft) option to a profile data structure 
 * 		based on a given string and string value.
 */
Profile *profile_add(Profile *p, char *opt, char *val) {
	assert(p);
	if (strcmp(opt, "deadline") == 0) {
		p->deadline = string_to_uint64(opt);
	} else if (strcmp(opt, "period") == 0) {
		p->period = string_to_uint64(opt);
	} else if (strcmp(opt, "implementationName") == 0) {
		strcpy_alloc(p->implementationName, val);
	}

	return p;
}

/*
 * Adds n modular redundancy to the given profile.
 */
Profile *profile_nModular_init(Profile *p) {
	assert(p);
	p->nModular = malloc(sizeof(nModularProfile));
	assert(p->nModular);
	return p;
}

/*
 * Adds n version programming to the given profile.
 */
Profile *profile_nVersion_init(Profile *p) {
	assert(p);
	p->nVersion = malloc(sizeof(nVersionProfile));
	p->nVersion->versions = list_init();
	assert(p->nVersion);
	return p;
}

/*
 * Adds primary backup / standby to the given profile.
 */
Profile *profile_standby_init(Profile *p) {
	assert(p);
	p->standby = malloc(sizeof(StandbyProfile));
	assert(p->standby);
	return p;
}

/*
 * Adds checkpoint restart to the given profile.
 */
Profile *profile_checkpoint_init(Profile *p) {
	assert(p);
	p->checkpoint = malloc(sizeof(CheckPointProfile));
	assert(p->checkpoint);
	return p;
}

/*
 * Adds a checkpoint restart option 
 * 		based on the given string equal to the given value.
 */
Profile *profile_checkpoint_add(Profile *p, char *opt, char *val) {
	assert(p && p->checkpoint);
	if (strcmp(opt, "shrinking") == 0) {
		if (strcmp(val, "true") == 0) {
			p->checkpoint->shrinking = true;
		} else {
			p->checkpoint->shrinking = false;
		}
	} else if (strcmp(opt, "frequency") == 0) {
		p->checkpoint->frequency = string_to_int(val);
	}

	return p;
}

/*
 * Adds a standby option 
 * 		based on the given string equal to the given value.
 */
Profile *profile_standby_add(Profile *p, char *opt, char *val) {
	assert(p && p->standby);
	if (strcmp(opt, "replicas") == 0) {
		p->standby->replicas = string_to_int(val);
	} else if (strcmp(opt, "frequency")) {
		if (strcmp(val, "cold") == 0) {
			p->standby->sync = cold;
		} else {
			p->standby->sync = hot; 
		}
	}

	return p;
}

/*
 * Adds a n version programming option 
 * 		based on the given string equal to the given value.
 */
Profile *profile_nVersion_add(Profile *p, char *opt, char *val) {
	assert(p && p->nVersion->versions);
	VersionItem *item = malloc(sizeof(VersionItem *));
	assert(item);
	strcpy_alloc(item->versionName, opt);
	item->replicas = string_to_int(val);
	list_add_back(p->nVersion->versions, item);
	return p;
}

/*
 * Adds a n modular redundancy option 
 * 		based on the given string equal to the given value.
 */
Profile *profile_nModular_add(Profile *p, char *opt, char *val) {
	assert(p && p->nModular);
	if (strcmp(opt, "replicas") == 0) {
		p->nModular->replicas = string_to_int(val);
	} else if (strcmp(opt, "votingReplicas") == 0) {
		p->nModular->votingReplicas = string_to_int(val);
	} else if (strcmp(opt, "waitingTime") == 0) {
		p->nModular->waitingTime = string_to_int(val);
	} else if (strcmp(opt, "waitingJoin") == 0) {
		if (strcmp(val, "true") == 0) {
			p->nModular->waitingJoin = true;
		} else {
			p->nModular->waitingJoin = false; 
		}
	} else if (strcmp(opt, "waitingStart") == 0) {
		if (strcmp(val, "single") == 0) {
			p->nModular->waitingStart = single;
		} else if (strcmp(val, "majority")) {
			p->nModular->waitingStart = majority; 
		}
	}

	return p;
}


/*
 * -------------------- Getters ------------------------------
 */

/*
 * Retrieves the number of replicas instantiated on this profile
 * TODO add n version programming here.
 */
int profile_get_num_replicas(Profile *p) {
	if (p->nModular) {
		return p->standby->replicas;
	} else if (p->standby) {
		return p->standby->replicas;
	}

	return 1;
}

/* 
 * Is checkpoint instantiated on this profile?
 */
bool profile_get_checkpoint(Profile *p) {
	return (p->checkpoint != NULL);
}

/* 
 * Is primary backup instantiated on this profile?
 */
bool profile_get_standby(Profile *p) {
	return (p->standby != NULL);
}
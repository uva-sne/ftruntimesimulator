/*
 * Copyright (C) 2020 Wouter Loeve <wouterloeve0@gmail.com>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "taskgraph.h"
#include <stdio.h>

#include "../Utils/pthreads.h"
#include "../Utils/cleanup.h"
#include "./profile.h"

#define EDGE_MODE_PREV 0
#define EDGE_MODE_NEXT 1
#define BUFFER_SIZE 20

/*
 * TaskGraph build and retrieve functions
 * The taskgraph consists of GraphStructs which are the nodes or components in 
 *      the DAG of the coordination application.
 */

/*
 * Initialise task graph with given size.
 */
task_graph *task_graph_init(int num_tasks) {
    task_graph *g = malloc(sizeof(task_graph));
    assert(g);
    g->numEdges = 0;
    g->compsIndex = 0;
    g->numComps = num_tasks;
    g->taskList = malloc(sizeof(GraphStruct *) * num_tasks);
    assert(g->taskList);
    g->roots = list_init();
    return g;
}

/*
 * Prints an edgestruct with portnames and nodes connected to it.
 */
void task_graph_print_edge(EdgeStruct *edge) {
    printf("EdgeStruct - %s\n", edge->inport);

    OutportStruct *curOutport = NULL;
    for (int i = 0; i < edge->outputIndex; i++) {
        curOutport = edge->outports[i];
        printf("Outport - Dest: %s, portname: %s\n", curOutport->graph->componentName, curOutport->portname);
        task_graph_print_GS(curOutport->graph);
    }
}

/*
 * Prints an entire task graph by calling print on nodes and edges inside.
 */
void task_graph_print(task_graph *g) {
    GraphStruct *curGS = NULL;
    list_node *curNode = task_graph_get_roots(g);

    for (int i = 0; i < task_graph_get_num_roots(g); i++) {
        curGS = list_get_data(curNode);
        task_graph_print_GS(curGS);

        curNode = list_get_next(curNode);
    }
}

/* 
 * Prints a graphstruct (node) in the task graph.
 */
void task_graph_print_GS(GraphStruct *gs) {
    printf("GS - id: %i, name: %s\n", gs->componentId, gs->componentName);
    for (int i = 0; i < gs->numInports; i++) {
        printf("inport %i: %s\n", i, gs->inport[i]);
    }

    printf("prev edges:\n");
    list_node *curNode = list_get_first(gs->prevEdges);
    EdgeStruct *curEdge = NULL;

    printf("next edges:\n");
    curNode = list_get_first(gs->nextEdges);
    curEdge = NULL;
    for (int i = 0; i < gs->nextEdges->size; i++) {
        curEdge = list_get_data(curNode);

        task_graph_print_edge(curEdge);

        curNode = list_get_next(curNode);
    }
}

/*
 * Creates a graphstruct (node in the task graph) with the properties given to it. 
 */
GraphStruct *task_graph_create_gs(char *name, int id, char** inports, int numInports) {
	GraphStruct *n = malloc(sizeof(GraphStruct));
    assert(n);
    n->componentName = strcpy_alloc(n->componentName, name);
	n->componentId = id;
    n->prevEdges = list_init();
    n->nextEdges = list_init();
    n->inQueue = false;
    n->inReady = false;
    n->profile = profile_init();
    n->threadId = NULL;
    // Will be overwritten when threads are set
    n->threadIdIndex = 0;
    n->threadIdSize = 0;
    n->nodeMutex = createPthreadMutex();

    n->inport = malloc(sizeof(char *) * numInports);
    assert(n->inport);

    for (int i = 0; i < numInports; i++) {
        n->inport[i] = strcpy_alloc(n->inport[i], inports[i]);
        DEBUG("create gs", n->inport[i]);
    }
    n->numInports = numInports;
	return n;
}

/*
 * Creates an outport struct which is connected to a graphstruct.
 * The outport struct then has an array of edges.
 */
OutportStruct *task_graph_create_os(EdgeStruct *edge, int multiplicity, char *portname, GraphStruct *cur) {
    OutportStruct *os = malloc(sizeof(OutportStruct));
    os->multiplicity = multiplicity;
    os->portname = strcpy_alloc(os->portname, portname);
    os->buffer = circ_buf_init(BUFFER_SIZE);
    os->checkpointBuffer = NULL;
    os->currdata = malloc(sizeof(void *) * multiplicity);
    os->edge = edge;
    os->graph = cur;
    
    assert(os->currdata);

    return os;
}

/*
 * Retrieves a graph struct by its name.
 * TODO build hash map or similar.
 */
GraphStruct *task_graph_get_gs_by_name(task_graph *g , char *name) {
    GraphStruct *cur = NULL;
    for (int i = 0; i < g->compsIndex; i++) {
        cur = g->taskList[i];
        if (strcmp(cur->componentName, name) == 0) {
            return cur;
        }
    }

    return NULL;
}

/*
 * Retrurns a graphstruct based on its id.
 */
GraphStruct *task_graph_get_gs_by_id(task_graph *g, int id) {
    return g->taskList[id];
}

/*
 * Creates an edge with the given properties.
 */
EdgeStruct *task_graph_create_edge(char* outport, char *inport, int numOutports, GraphStruct *input, int multiplicity, GraphStruct *output) {
    EdgeStruct *e = malloc(sizeof(EdgeStruct));
    assert(e);

    e->inport = strcpy_alloc(e->inport, inport);
    assert(e->inport);
    e->mutex = createPthreadMutex();
    e->outports = malloc(numOutports * sizeof(OutportStruct *));
    assert(e->outports);
    e->outputSize = numOutports;
    e->outputIndex = 0;
    e->outports[e->outputIndex] = task_graph_create_os(e, multiplicity, outport, output);
    // e->outports[e->outputIndex]->edge = e;

    e->input = input;

    return e;
}

/*
 * Retrieves the number of replicas of this node (component.)
 */
int task_graph_get_num_replicas(GraphStruct *gs) {
    assert(gs->profile);
    if (gs->profile->nModular != NULL) {
        return gs->profile->nModular->replicas;
    }
    if (gs->profile->standby != NULL) {
        // Primary is not counted in replicas
        return gs->profile->standby->replicas + 1;
    }
    if (gs->profile->nVersion != NULL) {
        // TODO do nversion.
        // for ()
        // return gs->profile->nVersion->versions
    }

    return 1;
}

/*
 * Sets the thread_id of the graph struct to the specified value.
 * This is done during reconfiguration as it specifies which 
 *      thread should execute the component.
 */
void task_graph_set_thread_id(GraphStruct *gs, int threadId) {
    if (gs->threadId == NULL) {
        // Create struct
        int replicas = task_graph_get_num_replicas(gs);
        gs->threadId = malloc(sizeof(int) * replicas);
        assert(gs->threadId);
        gs->threadIdIndex = 0;
        gs->threadIdSize = replicas;
    }
    // fprintf(stderr, "Componentname: %s, ThreadId %i, requiredAssignments %i\n", gs->componentName, gs->threadIdIndex, gs->threadIdSize);
    assert(gs->threadIdIndex != gs->threadIdSize);
    gs->threadId[gs->threadIdIndex] = threadId;
    gs->threadIdIndex++;
}

/*
 * Replaces the specified currently executing thread and replaces it with the new thread id.
 * Is used during reconfiguration when the current thread id has died.
 */
void task_graph_recover(GraphStruct *gs, int curThreadId, int newThreadId) {
    for (int i = 0; i < gs->threadIdSize; i++) {
        if (gs->threadId[i] == curThreadId) {
            gs->threadId[i] = newThreadId;
            return;
        }
    }

    ERROR("taskgraph.c", "Could not recover task");
}

/*
 * Adds a root to the task graph.
 */
void task_graph_add_root(task_graph *g, GraphStruct *cur) {
    assert (g && cur);
    list_add_back(g->roots, cur);
    g->taskList[g->compsIndex] = cur;
    g->compsIndex++;
}

/*
 * Retrieves the edge of the taskgraph by using the source port of the edge.
 * TODO use (hash)map or similar instead of loop here
 */
EdgeStruct *task_graph_get_edge(GraphStruct *prev, char *edgeInport) {
    list_node *curNode = list_get_first(prev->nextEdges);
    EdgeStruct *cur;
    for (int i = 0; i < prev->nextEdges->size; i++) {
        cur = list_get_data(curNode);

        if (strcmp(cur->inport, edgeInport) == 0) {
            return cur;
        } 
        curNode = list_get_next(curNode);
    }
    return NULL;
}

/*
 * Adds an outport to an edgestruct.
 */
void task_graph_add_outport(EdgeStruct *e, GraphStruct *cur, int multiplicity, char *outport) {
    OutportStruct *curOutport = task_graph_create_os(e, multiplicity, outport, cur);
    if (task_graph_outport_has_checkpoint(curOutport)) {
        // It also has to hold the current item until the component finishes.
        curOutport->checkpointBuffer = circ_buf_init(BUFFER_SIZE + 1);
    }
    e->outports[e->outputIndex] = curOutport;
    e->outputIndex++;
}


/*
 * Adds a component to an edge struct.
 */
void task_graph_add_comp(task_graph *g, GraphStruct *prev, GraphStruct *cur, char *outport, int multiplicity, char *inport, int numOutports) {
    EdgeStruct *dup = task_graph_get_edge(prev, inport);
    if (dup != NULL) {
        // Is duplicate edge
        task_graph_add_outport(dup, cur, multiplicity, outport);
        list_add_back(cur->prevEdges, dup);
    } else {
        task_graph_add_edge(g, prev, cur, outport, multiplicity, inport, numOutports);
    }
    // Add component
    assert(g->compsIndex < g->numComps);
    g->taskList[g->compsIndex] = cur;
    g->compsIndex++;
}

/*
 * Adds an edge to the task graph between the two given GraphStructs.
 */
void task_graph_add_edge(task_graph *g, GraphStruct *prev, GraphStruct *cur, char *outport, int multiplicity, char *inport, int numOutports) {
    EdgeStruct *e = task_graph_create_edge(outport, inport, numOutports, prev, multiplicity, cur);
    assert(e);
    list_add_back(cur->prevEdges, e);
    list_add_back(prev->nextEdges, e);
    task_graph_add_outport(e, cur, multiplicity, outport);
    
    g->numEdges++;
}

/*
 * Does the graphstruct connected with the outport (not via the edge but directly) have checkpoint enabled.
 */
bool task_graph_outport_has_checkpoint(OutportStruct *os) {
    return profile_get_checkpoint(os->graph->profile);
}

/*
 * Graph retrieval / adding tokens - functions
 */

/*
 * Adds the given void pointer to the buffer located on the edge coming from node with its name equal to outport.
 * If checkpoint restart is enabled, a valid copy function which copies the data should be passed as an argument.
 * Furthermore a timestruct can be given which times the overhead of checkpoint restart.
 */
void task_graph_add_to_buffer(GraphStruct *node, void *data, char *outport, void *(*copy_func)(void *), threadTimeStruct *time) {
    assert(node);
    list_linked *edges = node->nextEdges;
    EdgeStruct *cur = NULL;
    list_node *curNode = list_get_first(edges);
    OutportStruct *curOutport = NULL;
    for (int i = 0; i < edges->size; i++) {
        cur = list_get_data(curNode);
        assert(cur);
        // Check whether the outport of the component is equal to the inport of this edge.
        if (strcmp(outport, cur->inport) != 0) {
            curNode = list_get_next(curNode);
            continue;
        }

        // Add the token to all outports of this edge.
        int e = pthread_mutex_lock(cur->mutex);
        handleMutexError(cur->mutex, e);
            pthread_cleanup_push(mutex_cleanup_handler, cur->mutex);

            for (int port = 0; port < cur->outputSize; port++) {
                curOutport = cur->outports[port];

                // fprintf(stderr, "Adding to: Component %s edge inport %s - Buffer size: %i\n",  node->componentName, cur->inport, circ_buf_size(curOutport->buffer));
                assert(circ_buf_add(curOutport->buffer, data) == 0);

                // If checkpoint restart is specified, add it to the backup buffer as well.
                if (curOutport->checkpointBuffer) {
                    time_startCheckpoint(time);
                    
                    void *copy = (*copy_func)(data);
                    assert(circ_buf_add(curOutport->checkpointBuffer, copy) == 0);
                    
                    time_stopCheckpointRestart(time);
                }
            }
        // Unlock mutex
        pthread_cleanup_pop(1);
        return;
    }
}

/*
 * Creates a task with the given attributes.
 * Used in generated code.
 */
TaskStruct *task_graph_create_task(int componentId, char *componentName, void *inputData, void *outputData) {
    TaskStruct* task = malloc(sizeof(TaskStruct));
    assert(task);
    task->componentId = componentId;
    task->componentName = componentName;
    task->inputData = inputData;
    task->outputData = outputData;
    task->done = false;
    task->delivered = malloc(sizeof(int));
    assert(task->delivered);
    task->crashed = malloc(sizeof(int));
    assert(task->crashed);
    task->checkpointed = malloc(sizeof(bool));
    assert(task->crashed);
    *task->delivered = 0;
    *task->crashed = 0;
    *task->checkpointed = false;

    return task;
}

/*
 * Copy task with different input and output data.
 */
TaskStruct *task_graph_copy_task(TaskStruct *task, void *inputData, void *outputData) {
    TaskStruct *copy = task_graph_create_task(task->componentId, task->componentName, inputData, outputData);
    copy->delivered = task->delivered;
    copy->crashed = task->crashed;
    copy->checkpointed = task->checkpointed;
    return copy;
}

/*
 * Is the graph struct ready to fire.
 * A component needs to meet the following requirements to be eglible for firing:
 *  - All buffers connected on the outports of the node need to fit tokens equal to the production multiplicity of that port/edge/
 *  - All buffers connected to the inports of the node need to have at least a number of tokens equal to the consumption multiplicity of that port/edge.
 * 
 */
bool task_graph_ready_to_fire(GraphStruct *node) {
    assert(node);
    EdgeStruct *cur = NULL;
    list_node *curNode = list_get_first(node->prevEdges);
    OutportStruct *curOutport = NULL;
    /*
     * Loop trough predecessors of node
     */
    for (int i = 0; i < node->prevEdges->size; i++) {
        cur = list_get_data(curNode);
        int e = pthread_mutex_lock(cur->mutex);
        handleMutexError(cur->mutex, e);
        assert(cur && cur->outputSize == cur->outputIndex);

        // Check whether the multiplicity conditions are met for each edge
        for (int out = 0; out < cur->outputSize; out++) {
            for (int in = 0; in < node->numInports; in++) {
                curOutport = cur->outports[out];
                // Inport of the component needs to be an outport of this edge.
                
                if (strcmp(node->inport[in], curOutport->portname) == 0) {
                    if (circ_buf_size(curOutport->buffer) < curOutport->multiplicity) {
                        // fprintf(stderr, "Buffer too small %s.%s , size: %i\n", node->componentName, curOutport->portname, circ_buf_size(curOutport->buffer));
                        pthread_mutex_unlock(cur->mutex);
                        return false;
                    }
                    // fprintf(stderr, "Buffer size adequate %s.%s , size: %i, mult: %i\n", node->componentName, curOutport->portname, circ_buf_size(curOutport->buffer), curOutport->multiplicity);
                }
            }
        }
        pthread_mutex_unlock(cur->mutex);
        curNode = list_get_next(curNode);
    }

    /* 
     * Loop trough successors of the node
     */
    curNode = list_get_first(node->nextEdges);
    for (int i = 0; i < node->nextEdges->size; i++) {
        cur = list_get_data(curNode);

        int e = pthread_mutex_lock(cur->mutex);
        handleMutexError(cur->mutex, e);
        // Check whether the multiplicity conditions are met for each successor edge
        for (int out = 0; out < cur->outputIndex; out++) {
            curOutport = cur->outports[out];
            if (circ_buf_can_take(curOutport->buffer, curOutport->multiplicity) == false) {
                // fprintf(stderr, "FULL outport: %s - %s \n\n", curOutport->portname, node->componentName);
                pthread_mutex_unlock(cur->mutex);
                return false;
            }
        }
        pthread_mutex_unlock(cur->mutex);
        curNode = list_get_next(curNode);
    }
    return true;
}

/*
 * Retrieves the outport struct by port name.
 * TODO use hashmap or similar.
 */
OutportStruct *task_graph_get_OS_by_port(char *port, list_linked *edges) {
	OutportStruct *curOutPort = NULL;
	EdgeStruct *cur = NULL;
    list_node *curNode = list_get_first(edges);
	for (int i = 0; i < edges->size; i++) {
		cur = list_get_data(curNode);
		for (int out = 0; out < cur->outputSize; out++) {
			curOutPort = cur->outports[out];
			assert(curOutPort);
			if (strcmp(curOutPort->portname, port) == 0) {
				return curOutPort;
			}
		}
        curNode = list_get_next(curNode);
	}

    return NULL;
}

/*
 * Pops data tokens from the buffer located on the edge corresponding to the buffer
 * TODO use hash map.
 * Todo put timing here
 */
void **task_graph_get_data_by_port(char *port, list_linked *edges, bool checkpoint) {
    OutportStruct *curOutport = task_graph_get_OS_by_port(port, edges);
    assert(curOutport);
    int e = pthread_mutex_lock(curOutport->edge->mutex);
    handleMutexError(curOutport->edge->mutex, e);
        int numTokens = curOutport->multiplicity;
        void **result = curOutport->currdata;

        if (checkpoint == false) {
            assert(circ_buf_size(curOutport->buffer) >= numTokens);
        }

        // fprintf(stderr, "Buffer size %i %s\n", circ_buf_size(curOutport->buffer), port);
        for (int m = 0; m < numTokens; m++) {
            if (checkpoint) {
                result[m] = circ_buf_get(curOutport->checkpointBuffer);
            } else {
                result[m] = circ_buf_get(curOutport->buffer);
            }
            
            assert(result[m]);
        }
    pthread_mutex_unlock(curOutport->edge->mutex);
    return result;
}

/*
 * Returns an integer equal to the number of roots (i.e., source components) of the task graph.
 */
int task_graph_get_num_roots(task_graph *g) {
    return g->roots->size;
}

/*
 * Retrieves the roots of the task graph.
 */
list_node *task_graph_get_roots(task_graph *g) {
    return list_get_first(g->roots);
}

/*
 * Returns a boolean indicating whether the given GraphStruct is a root (i.e., source component).
 */
bool task_graph_is_root(GraphStruct *g) {
    return g->prevEdges->size == 0;
}
/*
 * Copyright (C) 2020 Wouter Loeve <wouterloeve0@gmail.com>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h> 

#include "main.h"
#include "./generated/generated.h"
#include "Utils/global_structs.h"
#include "Utils/cleanup.h"
#include "Utils/log.h"
#include "Utils/pthreads.h"
#include "Utils/print.h"
#include "ConfigParser/configparser.h"
#include "ConfigParser/threadConfigParser.h"
#include "./Datastructures/linkedlist.h"
#include "ThreadFunctions/runtime.h"
#include "ThreadFunctions/heartbeat.h"
#include "Datastructures/linkedlist.h"

// Global variable for cleanup
MainStruct *glob_mainInfo = NULL;

/*
 * Handle an artifically crashed thread by calling cleanup.
 */
void handle_sigint(int sig) {
    assert(glob_mainInfo);

    if (sig == SIGINT) {
        general_cleanup(glob_mainInfo);
        exit(1);
    }
}

/*
 * Prepare the datastructures and launch threads before simulation starts.
 */
MainStruct *initialise(SimulationConfig *config, ThreadConfig *thConfig) {
    MainStruct *mainInfo = initDatastructures(config, thConfig);
    mainInfo = initThreads(mainInfo);
    return mainInfo;
}

/*
 * Initialises the heartbeat configuration data structure.
 */
SimulationConfig *init_sim_config() {
    SimulationConfig *config = malloc(sizeof(SimulationConfig));
    assert(config);

    config->diagnosticsOutput = NULL;

    config->heartbeatWorkerPrio = 1;
    config->heartbeatCheckerPrio = 1;
    config->standbyEarlyTaskCompletion = true;

    return config;
}

/*
 * Parse the sleep and thread crash order arguments passed with the command line interface
 * -o: requires a list of integers which correspond to the thread crash order.
 *     Example: "-o 0,1,2,3,4"
 * -s: requires a list of integers corresponding to the number of seconds to wait between the crashes.
 *     Example: "-s 5,5,5,5,5"
 */
int *parseCLIlist(char *list, int *size) {
    char *found;
    char *s = NULL;
    list_linked *l = list_init();
    while( (found = strsep(&list,",")) != NULL ) {
        s = strcpy_alloc(s, found);
        list_add_back(l, s);
    }
    
    int *newList = malloc(sizeof(int) * l->size);
    assert(newList);
    char *cur = NULL;
    list_node *n = list_get_first(l);

    for (int i = 0; i < l->size; i++) {
        cur = list_get_data(n);
        newList[i] = (int) strtol(trimwhitespace(cur), (char **)NULL, 10);
        free(cur);
        n = list_get_next(n);
    }
    *size = l->size;
    free(l);
    return newList;
}

/*
 * The main function of the program. 
 * Parses command line arguments, calls initialiser functions for the datastructures and threads and finally executes the given thread crash order.
 */
int main(int argc, char *argv[]) {
    signal(SIGINT, handle_sigint); 
    int opt;
    char *configfile = malloc(sizeof(char) * 300);
    char *thConfigFile = malloc(sizeof(char) * 300);
    assert(configfile);
    bool printConfigFlag = false;
    int *crashOrder = NULL;
    int crashSize = 0;
    int sleepSize = 0;
    int *crashSleeps = NULL;
    // Default is config.txt
    strcpy(configfile, "config.txt");
    strcpy(thConfigFile, "threadConfig.txt");

    SimulationConfig *config = init_sim_config();

    while ((opt = getopt(argc, argv, "c:t:ho:s:p")) != -1) {
        switch (opt) {
        case 'c':
            // optarg is from getopt
            configfile = optarg;
            break;
        case 'p':
            printConfigFlag = true;
            break;
        case 'h':
            printHelp();
            exit(EXIT_SUCCESS);
        case 't':
            thConfigFile = optarg;
            break;
        case 'o':
            crashOrder = parseCLIlist(optarg, &crashSize);
            break;
        case 's':
            crashSleeps = parseCLIlist(optarg, &sleepSize);
            break;
        default:
            fprintf(stderr, "Usage: %s [OPTION] [FILE]\n", argv[0]);
            printHelp();
            exit(EXIT_FAILURE);
        }
    }
    if (crashSize != sleepSize) {
        ERROR("argument parser", "List of sleep sizes not equal to list of crash sizes");
    }

    if (configfile == NULL) {
        ERROR("argument parser", "Config file must be specified. This can be done with -c [file]\n");
    }
    config->thConfigFile = thConfigFile;
    config->configfile = configfile;
    config->printConfig = printConfigFlag;
    assert(config->thConfigFile && config->configfile);
    parseConfig(config);
    if (crashSize > config->numThreads) {
        ERROR("argument parser", "num crashes cannot be higher than num threads");
    }

    ThreadConfig *thConfig = parseThreadConfig(config);
    assert(thConfig);

    if (printConfigFlag) {
        printConfig(config);
        printThreadConfig(thConfig);
    }

    // Initialise datastructures, create taskgraph, run threads
    MainStruct *mainInfo = initialise(config, thConfig);
    glob_mainInfo = mainInfo;

    printAll(mainInfo, (mainInfo->diagnostics->logFile != NULL));

    for (int i = 0; i < crashSize; i ++) {
        sleep(crashSleeps[i]);
        assert(crashOrder[i] < config->numThreads);
        int threadIndex = mainInfo->commInfos[crashOrder[i]]->buddyId;
        int commIndex = mainInfo->commInfos[crashOrder[i]]->threadId;
        fprintf(stderr, "Crashing Thread %i - Comm thread %i\n", threadIndex, commIndex);
        
        pthread_cancel(*mainInfo->tids[threadIndex]);
        pthread_cancel(*mainInfo->tids[commIndex]);
        pthread_join(*mainInfo->tids[threadIndex], NULL);
        pthread_join(*mainInfo->tids[commIndex], NULL);
    }

    while (true) {
        sleep(1000);
    }
}
/*
 * Copyright (C) 2020 Wouter Loeve <wouterloeve0@gmail.com>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "copy.h"

#include <stdlib.h>
#include "./computation.h"
#include <assert.h>
#include <string.h>

/*
 * This file contains the copy functions of the data types of the implemention of the components.
 */

/* 
 * Copy an array of (32-bit) integers
 * Todo, find a way to give size variables here....
 */
void* copy_uint32_t_ptr(void *dist) {
	uint32_t *old = (uint32_t *) dist;
    uint32_t *d = malloc(sizeof(uint32_t) * ARRAY_SIZE);
	assert(d);
	memcpy(d, old, sizeof(uint32_t) * ARRAY_SIZE);
	// for (int i = 0; i < ARRAY_SIZE; i++) {
	// 	d[i] = old[i];
	// }

    return d;
}

/*
 * Copy an integer.
 * Every data structure needs 
 */
void *copy_uint32_t(void *a) {
	return a;
}
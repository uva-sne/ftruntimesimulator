/*
 * Copyright (C) 2020 Wouter Loeve <wouterloeve0@gmail.com>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef COMPUTATION_H
#define COMPUTATION_H

#include <stdint.h>
#include <unistd.h>

#define ARRAY_SIZE 1000
#define N_ADDITIONS 50000


/*
 * User implementation goes here.
 */

void DistSensor(uint32_t **dist);

void ImageCapture(uint32_t **frameData);

void Decision(uint32_t *dist, uint32_t *frameData, uint32_t **voltage1);

void LeftActuator(uint32_t *voltage);

void RightActuator(uint32_t *voltage);

/*
 * Errortokens:
 */
void Source(uint32_t *pair1, uint32_t *pair2);
void Sink(uint32_t pair1, uint32_t pair2);
void A(uint32_t pair1, uint32_t *pair1Out);
void B(uint32_t pair2, uint32_t *pair2Out);


#endif /* COMPUTATION_H */
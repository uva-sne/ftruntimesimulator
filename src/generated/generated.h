#ifndef GENERATED_H
#define GENERATED_H

#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "../ThreadFunctions/init.h"
#include "../Utils/global_structs.h"
#include "../Datastructures/taskgraph.h"
#include "../Datastructures/profile.h"
#include "../Utils/log.h"
#include "./copy.h"

void store_output(MainStruct *minfo, TaskStruct *currentTask, threadTimeStruct *time);

void compFunc(TaskStruct *currentTask);

task_graph *generate_task_graph();

TaskStruct *take_output(GraphStruct *node, list_linked *edges, MainStruct *mainInfo, void *errorToken, bool checkpoint);

void set_error(TaskStruct *currentTask);

void *alloc_output_struct(int compId);

void *copy_comp_data(void *data, int compId);

#endif /* GENERATED_H */
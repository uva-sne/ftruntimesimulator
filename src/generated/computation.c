/*
 * Copyright (C) 2020 Wouter Loeve <wouterloeve0@gmail.com>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "computation.h"

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

/*
 * This file contains the implementaton functions of the components.
 * TODO make this more modular.
 */

int printed = 0;

void add_addtest(int i1, int i2, int *out) {
	*out = i1 + i2;
}

void gen_1(int *out) {
	*out = 1;
}

void gen_2(int *out) {
	*out = 2;
}

void gen_3(int *out) {
	*out = 3;
}

void mul(int i2, int i1, int *out) {
	*out = i1 * i2;
}

void print(int i) {
	// fprintf(stderr, "PRINTING %i FOR %ith TIME\n\n", i, printed);
	// fprintf(stdout, "PRINTING %i FOR %ith TIME\n\n", i, printed);
	printed++;
}

/*
 * ******** Carsub example *************
 */

// Idea, take timestamp as frame, and then print them in actuator.
void DistSensor(uint32_t **dist) {
	uint32_t *d = malloc(sizeof(uint32_t) * ARRAY_SIZE);
	for (int i = 0; i < ARRAY_SIZE; i++) {
		d[i] = 0;
	}
	*dist = d;
}

void ImageCapture(uint32_t **frameData) {
	uint32_t *d = malloc(sizeof(uint32_t) * ARRAY_SIZE);
	for (int i = 0; i < ARRAY_SIZE; i++) {
		d[i] = 0;
	}
	*frameData = d;
}

void Decision(uint32_t *dist, uint32_t *frameData, uint32_t **voltage1) {
	for (uint32_t j = 0; j < ARRAY_SIZE; j++) {
		for (uint32_t i = 0; i < N_ADDITIONS; i++) {
			dist[j] += 1;
			frameData[j] += 1;
		}
	}

	uint32_t *voltage = malloc(sizeof(uint32_t) * ARRAY_SIZE);
	assert(voltage);
	for (uint32_t i = 0; i < ARRAY_SIZE; i++) {
		voltage[i] = dist[i] - frameData[i];
		if (dist[i] != frameData[i]) {
			printf("%i - %i \n", dist[i], frameData[i]);
		}
		assert(dist[i] == frameData[i]);
		assert(voltage[i] == 0);
	}
	*voltage1 = voltage;
}

void LeftActuator(uint32_t *voltage) {
	for (uint32_t i = 0; i < ARRAY_SIZE; i++) {
		if (voltage[i] != 0) {
			assert(voltage[i] == 0);
		}
	}
}

void RightActuator(uint32_t *voltage) {
	for (uint32_t i = 0; i < ARRAY_SIZE; i++) {
		if (voltage[i] != 0) {
			assert(voltage[i] == 0);
		}
	}
}

int iterations = 0;

/*
 * ******** Test errortokens example *************
 */
void Source(uint32_t *pair1, uint32_t *pair2) {
	*pair1 = iterations;
	*pair2 = iterations + 100;
	iterations++;
}

void Sink(uint32_t pair1, uint32_t pair2) {
	fprintf(stderr, "Sink Answer: %i - %i\n", pair1, pair2);
	assert(pair1 + 100 == pair2);
}

void A(uint32_t pair1, uint32_t *pair1Out) {
	*pair1Out = pair1;
}

void B(uint32_t pair2, uint32_t *pair2Out) {
	*pair2Out = pair2;
}/*
 * Returns an integer equal to the number of roots (source components) of the task graph.
 */
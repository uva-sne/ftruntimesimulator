#include "generated.h"
#include "computation.h"


typedef struct {
uint32_t * dist;
bool _error;
} DistSensor_out_struct;


typedef struct {
uint32_t * frameData;
bool _error;
} ImageCapture_out_struct;

typedef struct {
uint32_t * dist;
uint32_t * frameData;
bool _error;
} Decision_in_struct;

typedef struct {
uint32_t * voltage;
bool _error;
} Decision_out_struct;

typedef struct {
uint32_t * voltage1;
bool _error;
} LeftActuator_in_struct;


typedef struct {
uint32_t * voltage2;
bool _error;
} RightActuator_in_struct;


task_graph *generate_task_graph() {
task_graph *g = task_graph_init(5);
GraphStruct *DistSensor_gs = task_graph_create_gs("DistSensor", 0, NULL, 0);

GraphStruct *ImageCapture_gs = task_graph_create_gs("ImageCapture", 1, NULL, 0);

char *ports2[] = {"dist", "frameData"};
GraphStruct *Decision_gs = task_graph_create_gs("Decision", 2, ports2, 2);

char *ports3[] = {"voltage1"};
GraphStruct *LeftActuator_gs = task_graph_create_gs("LeftActuator", 3, ports3, 1);

char *ports4[] = {"voltage2"};
GraphStruct *RightActuator_gs = task_graph_create_gs("RightActuator", 4, ports4, 1);

profile_add(DistSensor_gs->profile, "deadline", "0ns");
profile_add(DistSensor_gs->profile, "period", "0ns");
profile_add(ImageCapture_gs->profile, "deadline", "0ns");
profile_add(ImageCapture_gs->profile, "period", "0ns");
profile_checkpoint_init(Decision_gs->profile);
profile_checkpoint_add(Decision_gs->profile, "shrinking", "false");
profile_add(Decision_gs->profile, "deadline", "0ns");
profile_add(Decision_gs->profile, "period", "0ns");
profile_add(LeftActuator_gs->profile, "deadline", "0ns");
profile_add(LeftActuator_gs->profile, "period", "0ns");
profile_add(RightActuator_gs->profile, "deadline", "0ns");
profile_add(RightActuator_gs->profile, "period", "0ns");
task_graph_add_root(g, DistSensor_gs);
task_graph_add_root(g, ImageCapture_gs);
task_graph_add_comp(g, DistSensor_gs, Decision_gs, "dist", 1, "dist", 1);
task_graph_add_edge(g, ImageCapture_gs, Decision_gs, "frameData", 1, "frameData", 1);
task_graph_add_comp(g, Decision_gs, LeftActuator_gs, "voltage1", 1, "voltage", 2);
task_graph_add_comp(g, Decision_gs, RightActuator_gs, "voltage2", 1, "voltage", 2);
return g;
}
void compFunc(TaskStruct *currentTask) {
	switch(currentTask->componentId) {
	case 0: {
		DistSensor_out_struct *out = (DistSensor_out_struct *) currentTask->outputData;
		DistSensor(&out->dist);
		break;
	}
	case 1: {
		ImageCapture_out_struct *out = (ImageCapture_out_struct *) currentTask->outputData;
		ImageCapture(&out->frameData);
		break;
	}
	case 2: {
		Decision_in_struct *in = (Decision_in_struct *) currentTask->inputData;
		Decision_out_struct *out = (Decision_out_struct *) currentTask->outputData;
		Decision(in->dist, in->frameData, &out->voltage);
		break;
	}
	case 3: {
		LeftActuator_in_struct *in = (LeftActuator_in_struct *) currentTask->inputData;
		LeftActuator(in->voltage1);
		break;
	}
	case 4: {
		RightActuator_in_struct *in = (RightActuator_in_struct *) currentTask->inputData;
		RightActuator(in->voltage2);
		break;
	}
	}
}
void store_output(MainStruct *minfo, TaskStruct *currentTask, threadTimeStruct *time) {
	int compId = currentTask->componentId;
	assert(compId <= minfo->taskGraph->compsIndex);
	GraphStruct *finished = minfo->taskGraph->taskList[compId];

	switch(compId) {
	case 0: {
		DistSensor_out_struct *out = (DistSensor_out_struct *) currentTask->outputData;

		if(out->_error) {
			task_graph_add_to_buffer(finished, minfo->errorToken, "dist", copy_uint32_t_ptr, time);
		} else {
			task_graph_add_to_buffer(finished, out->dist, "dist", copy_uint32_t_ptr, time);
		}
		break;
	}
	case 1: {
		ImageCapture_out_struct *out = (ImageCapture_out_struct *) currentTask->outputData;

		if(out->_error) {
			task_graph_add_to_buffer(finished, minfo->errorToken, "frameData", copy_uint32_t_ptr, time);
		} else {
			task_graph_add_to_buffer(finished, out->frameData, "frameData", copy_uint32_t_ptr, time);
		}
		break;
	}
	case 2: {
		Decision_out_struct *out = (Decision_out_struct *) currentTask->outputData;

		if(out->_error) {
			task_graph_add_to_buffer(finished, minfo->errorToken, "voltage", copy_uint32_t_ptr, time);
		} else {
			task_graph_add_to_buffer(finished, out->voltage, "voltage", copy_uint32_t_ptr, time);
		}
		break;
	}
	case 3: {
		break;
	}
	case 4: {
		break;
	}
	}
}
TaskStruct *take_output(GraphStruct *node, list_linked *edges, MainStruct *mainInfo, void *errorToken, bool checkpoint) {
	int compId = node->componentId;
	TaskStruct *task = NULL;

	bool error = false;
	switch(compId) {
	case 0: {
		DistSensor_out_struct *out = malloc(sizeof(DistSensor_out_struct));
		assert(out);
		out->_error = false;
		task = task_graph_create_task(0, "DistSensor", NULL, out);
		break;
	}
	case 1: {
		ImageCapture_out_struct *out = malloc(sizeof(ImageCapture_out_struct));
		assert(out);
		out->_error = false;
		task = task_graph_create_task(1, "ImageCapture", NULL, out);
		break;
	}
	case 2: {
		Decision_out_struct *out = malloc(sizeof(Decision_out_struct));
		assert(out);
		out->_error = false;
		Decision_in_struct *in = malloc(sizeof(Decision_in_struct));
		assert(in);
		void **dist_var = task_graph_get_data_by_port("dist", edges, checkpoint);
		assert(dist_var);
		if (dist_var == errorToken) {error = true; out->_error = true;}
		if (dist_var[0] == errorToken) {error = true; out->_error = true;}
		in->dist = *((uint32_t * *) dist_var); 
		void **frameData_var = task_graph_get_data_by_port("frameData", edges, checkpoint);
		assert(frameData_var);
		if (frameData_var == errorToken) {error = true; out->_error = true;}
		if (frameData_var[0] == errorToken) {error = true; out->_error = true;}
		in->frameData = *((uint32_t * *) frameData_var); 
		task = task_graph_create_task(2, "Decision", in, out);
		break;
	}
	case 3: {
		LeftActuator_in_struct *in = malloc(sizeof(LeftActuator_in_struct));
		assert(in);
		void **voltage1_var = task_graph_get_data_by_port("voltage1", edges, checkpoint);
		assert(voltage1_var);
		if (voltage1_var == errorToken) {error = true; }
		if (voltage1_var[0] == errorToken) {error = true; }
		in->voltage1 = *((uint32_t * *) voltage1_var); 
		task = task_graph_create_task(3, "LeftActuator", in, NULL);
		break;
	}
	case 4: {
		RightActuator_in_struct *in = malloc(sizeof(RightActuator_in_struct));
		assert(in);
		void **voltage2_var = task_graph_get_data_by_port("voltage2", edges, checkpoint);
		assert(voltage2_var);
		if (voltage2_var == errorToken) {error = true; }
		if (voltage2_var[0] == errorToken) {error = true; }
		in->voltage2 = *((uint32_t * *) voltage2_var); 
		task = task_graph_create_task(4, "RightActuator", in, NULL);
		break;
	}
	}
	if (error) { store_output(mainInfo, task, mainInfo->diagnostics->time); return NULL; }
	return task;
}
void set_error(TaskStruct *currentTask) {
	int compId = currentTask->componentId;
	switch(compId) {
	case 0: {
		DistSensor_out_struct *out = (DistSensor_out_struct *) currentTask->outputData;
		out->_error = true;
		break;
	}
	case 1: {
		ImageCapture_out_struct *out = (ImageCapture_out_struct *) currentTask->outputData;
		out->_error = true;
		break;
	}
	case 2: {
		Decision_out_struct *out = (Decision_out_struct *) currentTask->outputData;
		out->_error = true;
		break;
	}
	case 3: {
		break;
	}
	case 4: {
		break;
	}
	}
}
void *copy_comp_data(void *data, int compId) {
	switch(compId) {
	case 0: {
		return NULL;
	}
	case 1: {
		return NULL;
	}
	case 2: {
		Decision_in_struct *in = (Decision_in_struct *) data;
		Decision_in_struct *in_copy = malloc(sizeof(Decision_in_struct));
		assert(in);
		in_copy->dist = copy_uint32_t_ptr(in->dist);
		in_copy->frameData = copy_uint32_t_ptr(in->frameData);
		return in_copy;
	}
	case 3: {
		LeftActuator_in_struct *in = (LeftActuator_in_struct *) data;
		LeftActuator_in_struct *in_copy = malloc(sizeof(LeftActuator_in_struct));
		assert(in);
		in_copy->voltage1 = copy_uint32_t_ptr(in->voltage1);
		return in_copy;
	}
	case 4: {
		RightActuator_in_struct *in = (RightActuator_in_struct *) data;
		RightActuator_in_struct *in_copy = malloc(sizeof(RightActuator_in_struct));
		assert(in);
		in_copy->voltage2 = copy_uint32_t_ptr(in->voltage2);
		return in_copy;
	}
	}
return NULL;
}
void *alloc_output_struct(int compId) {
	switch(compId) {
	case 0: {
		DistSensor_out_struct *out = malloc(sizeof(DistSensor_out_struct));
		assert(out);
out->_error = false;
		return out;
	}
	case 1: {
		ImageCapture_out_struct *out = malloc(sizeof(ImageCapture_out_struct));
		assert(out);
out->_error = false;
		return out;
	}
	case 2: {
		Decision_out_struct *out = malloc(sizeof(Decision_out_struct));
		assert(out);
out->_error = false;
		return out;
	}
	case 3: {
		return NULL;
	}
	case 4: {
		return NULL;
	}
	}
return NULL;
}
